namespace Base2art.ComponentModel.Composition
{
    using System;

    /// <summary>
    ///     The resolving injector.
    /// </summary>
    public interface IServiceLoaderInjector
    {
        /// <summary>
        ///     Resolves an object by type.
        /// </summary>
        /// <param name="contractType">The type of object to return.</param>
        /// <returns>The resolved object.</returns>
        object Resolve(Type contractType);

        /// <summary>
        ///     Resolves an object by type.
        /// </summary>
        /// <typeparam name="T">The type of object to return.</typeparam>
        /// <returns>The resolved object.</returns>
        T Resolve<T>();

        /// <summary>
        ///     Resolves an object by type.
        /// </summary>
        /// <param name="contractType">The type of object to return.</param>
        /// <param name="name">The registration name of the bindign to resolve.</param>
        /// <returns>The resolved object.</returns>
        object ResolveByNamed(Type contractType, string name);

        /// <summary>
        ///     Resolves an object by type.
        /// </summary>
        /// <typeparam name="T">The type of object to return.</typeparam>
        /// <param name="name">The registration name of the bindign to resolve.</param>
        /// <returns>The resolved object.</returns>
        T ResolveByNamed<T>(string name);

        /// <summary>
        ///     Resolves a list of objects by type.
        /// </summary>
        /// <param name="contractType">The type of objects to return.</param>
        /// <returns>The list of resolved object.</returns>
        object[] ResolveAll(Type contractType);

        /// <summary>
        ///     Resolves a list of objects by type.
        /// </summary>
        /// <typeparam name="T">The type of objects to return.</typeparam>
        /// <returns>The list of resolved object.</returns>
        T[] ResolveAll<T>();

        /// <summary>
        ///     Resolves an object by type.
        /// </summary>
        /// <param name="contractType">The type of object to return.</param>
        /// <param name="returnDefaultOnNotFound">
        ///     A value indicating that a null value should be returned (as opposed to throwing an exception)
        ///     when resolution fails.
        /// </param>
        /// <returns>The resolved object.</returns>
        object Resolve(Type contractType, bool returnDefaultOnNotFound);

        /// <summary>
        ///     Resolves an object by type.
        /// </summary>
        /// <typeparam name="T">The type of object to return.</typeparam>
        /// <param name="returnDefaultOnNotFound">
        ///     A value indicating that a null value should be returned (as opposed to throwing an exception)
        ///     when resolution fails.
        /// </param>
        /// <returns>The resolved object.</returns>
        T Resolve<T>(bool returnDefaultOnNotFound);

        /// <summary>
        ///     Resolves an object by type.
        /// </summary>
        /// <param name="contractType">The type of object to return.</param>
        /// <param name="name">The registration name of the bindign to resolve.</param>
        /// <param name="returnDefaultOnNotFound">
        ///     A value indicating that a null value should be returned (as opposed to throwing an exception)
        ///     when resolution fails.
        /// </param>
        /// <returns>The resolved object.</returns>
        object ResolveByNamed(Type contractType, string name, bool returnDefaultOnNotFound);

        /// <summary>
        ///     Resolves an object by type.
        /// </summary>
        /// <typeparam name="T">The type of object to return.</typeparam>
        /// <param name="name">The registration name of the bindign to resolve.</param>
        /// <param name="returnDefaultOnNotFound">
        ///     A value indicating that a null value should be returned (as opposed to throwing an exception)
        ///     when resolution fails.
        /// </param>
        /// <returns>The resolved object.</returns>
        T ResolveByNamed<T>(string name, bool returnDefaultOnNotFound);

        /// <summary>
        ///     Resolves a list of objects by type.
        /// </summary>
        /// <param name="contractType">The type of objects to return.</param>
        /// <param name="returnDefaultOnNotFound">
        ///     A value indicating that a null value should be returned (as opposed to throwing an exception)
        ///     when resolution fails.
        /// </param>
        /// <returns>The list of resolved object.</returns>
        object[] ResolveAll(Type contractType, bool returnDefaultOnNotFound);

        /// <summary>
        ///     Resolves a list of objects by type.
        /// </summary>
        /// <typeparam name="T">The type of objects to return.</typeparam>
        /// <param name="returnDefaultOnNotFound">
        ///     A value indicating that a null value should be returned (as opposed to throwing an exception)
        ///     when resolution fails.
        /// </param>
        /// <returns>The list of resolved object.</returns>
        T[] ResolveAll<T>(bool returnDefaultOnNotFound);
    }
}