namespace Base2art.ComponentModel.Composition
{
    using System;

#if NETSTANDARD2_0
    using System.Runtime.Serialization;
#endif

    /// <summary>
    ///     An exception used when binding a type doesn't meet all satisfying properties.
    /// </summary>
#if NETSTANDARD2_0
    [Serializable]
#endif
    public class CircularDependencyException : Exception
    {
        /// <summary>Initializes a new instance of the <see cref="T:System.Exception" /> class.</summary>
        public CircularDependencyException()
        {
        }

        /// <summary>Initializes a new instance of the <see cref="T:System.Exception" /> class with a specified error message.</summary>
        /// <param name="message">The message that describes the error. </param>
        public CircularDependencyException(string message)
            : base(message)
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="T:System.Exception" /> class with a specified error message and a
        ///     reference to the inner exception that is the cause of this exception.
        /// </summary>
        /// <param name="message">The error message that explains the reason for the exception. </param>
        /// <param name="innerException">
        ///     The exception that is the cause of the current exception, or a null reference (Nothing in
        ///     Visual Basic) if no inner exception is specified.
        /// </param>
        public CircularDependencyException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

#if NETSTANDARD2_0
/// <summary>
///     Serialization exception.
/// </summary>
/// <param name="info">The info parameter.</param>
/// <param name="context">The context parameter.</param>
        protected CircularDependencyException(SerializationInfo info,
                                              StreamingContext context)
            : base(info, context)
        {
        }
#endif
    }
}