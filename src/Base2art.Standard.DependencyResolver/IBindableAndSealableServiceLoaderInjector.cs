﻿namespace Base2art.ComponentModel.Composition
{
    /// <summary>
    ///     A contract that allows users to register bindings and seal the binder as readonly.
    /// </summary>
    public interface IBindableAndSealableServiceLoaderInjector : IBindableServiceLoaderInjector
    {
        /// <summary>
        ///     (DO NOT USE, IN PRODUCTION It RUINS LAZY LOADING)
        ///     This method runs through all bindings and creates an instance of all bindings.
        ///     This is useful for development to verify that you have everything configured correctly.
        /// </summary>
        void VerifyAll();

        /// <summary>
        ///     Marks the container as readonly.
        ///     After it is sealed, exceptions will be thrown if you try to bind additional items.
        /// </summary>
        void Seal();
    }
}