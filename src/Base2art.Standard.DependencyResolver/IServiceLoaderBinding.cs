﻿namespace Base2art.ComponentModel.Composition
{
    using System;
    using System.Diagnostics.CodeAnalysis;

    /// <summary>
    ///     A contract that allows consumers to complete the binding in the service loader.
    /// </summary>
    /// <typeparam name="T">The item being bound (typically an interface).</typeparam>
    public interface IServiceLoaderBinding<in T>
    {
        /// <summary>
        ///     A way to indicating that the item should be registered as singleton or per-instance.
        /// </summary>
        /// <param name="type">The value to be bound.</param>
        /// <returns>The current binding.</returns>
        IServiceLoaderBinding<T> As(ServiceLoaderBindingType type);

        /// <summary>
        ///     A way to name the binding for later recall.
        /// </summary>
        /// <param name="name">The name of the registration.</param>
        /// <returns>The current binding.</returns>
        IServiceLoaderBinding<T> Named(string name);

        /// <summary>
        ///     A method that completes the registration.
        /// </summary>
        /// <param name="creationFunction">A function that returns the implementation.</param>
        void To(Func<IServiceLoaderInjector, T> creationFunction);

        /// <summary>
        ///     A method that completes the registration by mapping to a concrete instance.
        /// </summary>
        /// <param name="concreteValue">The instace that is returned when Resolve() is invoked.</param>
        /// <typeparam name="TConcrete">The type of the object being bound.</typeparam>
        void To<TConcrete>(TConcrete concreteValue)
            where TConcrete : T;

        /// <summary>
        ///     Complete the registration by mapping the binding to the a concrete instance.
        /// </summary>
        /// <typeparam name="TConcrete">The type of object to return when Resolve() is invoked.</typeparam>
        [SuppressMessage("Microsoft.Design", "CA1004:GenericMethodsShouldProvideTypeParameter", Justification = "SjY")]
        void ToInstanceCreator<TConcrete>()
            where TConcrete : T;
    }
}