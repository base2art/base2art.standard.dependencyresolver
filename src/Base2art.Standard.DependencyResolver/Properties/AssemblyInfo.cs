using System.Reflection;
using System.Runtime.CompilerServices;

[assembly: AssemblyTitle("Base2art.DependencyResolver")]
[assembly: AssemblyDescription("Base2art simple component library")]
[assembly: AssemblyConfiguration("")]

[assembly: InternalsVisibleTo("Base2art.Standard.DependencyResolver.Features")]