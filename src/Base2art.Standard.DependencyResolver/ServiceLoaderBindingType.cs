﻿namespace Base2art.ComponentModel.Composition
{
    /// <summary>
    ///     An enumeration that indicates the type of binding.
    /// </summary>
    public enum ServiceLoaderBindingType
    {
        /// <summary>
        ///     Indicates that every time you use the service loader to fetch a
        ///     Type registered as "Instance" you will get a new instance.
        /// </summary>
        Instance,

        /// <summary>
        ///     Indicates that no matter how many times you use the service
        ///     loader to fetch a Type registered as Singleton, you will retrieve
        ///     a shared instance.
        /// </summary>
        Singleton
    }
}