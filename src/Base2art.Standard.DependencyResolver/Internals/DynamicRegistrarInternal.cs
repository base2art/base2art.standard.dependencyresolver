﻿namespace Base2art.ComponentModel.Composition
{
    using System;
    using System.Collections.Generic;
    using System.Reflection;

    internal static class DynamicRegistrarInternal
    {
        internal static readonly Type[] Register1NonGenericParameterTypes =
        {
            typeof(IBindableServiceLoaderInjector),
            typeof(ServiceLoaderBindingType)
        };

        internal static readonly Type[] Register2NonGenericParameterTypes =
        {
            typeof(IBindableServiceLoaderInjector),
            typeof(IServiceLoaderBinding<>),
            typeof(IDictionary<string, object>),
            typeof(IDictionary<string, object>)
        };

        internal static readonly Type[] Register3NonGenericParameterTypes =
        {
            typeof(IServiceLoaderBinding<>),
            typeof(object)
        };

        public static T CreateFrom<T>(
            this IServiceLoaderInjector config,
            IDictionary<string, object> inputParameters,
            IDictionary<string, object> properties)
        {
            var obj = (T) typeof(T).CreateFromInternal(config,
                                                       s => inputParameters.ContainsKey(s.Name),
                                                       s => Creator.Convert(inputParameters, () => s.Name, () => s.ParameterType));
            Creator.SetProperties(properties, obj);
            return obj;
        }

        internal static IEnumerable<Type> GetAllInterfacesImplementedBy<T>()
        {
            return typeof(T).GetTypeInfo().ImplementedInterfaces;
        }

        public static IServiceLoaderBinding<T> Register1NonGeneric<T>(
            IBindableServiceLoaderInjector injector,
            ServiceLoaderBindingType bindingType,
            string named)
        {
            if (string.IsNullOrWhiteSpace(named))
            {
                return injector.Bind<T>().As(bindingType);
            }

            return injector.Bind<T>().As(bindingType).Named(named);
        }

        public static void Register2NonGeneric<T, TConcrete>(
            IServiceLoaderBinding<T> binding,
            IDictionary<string, object> inputParms,
            IDictionary<string, object> properties)
            where TConcrete : T
        {
            binding.To(injector1 => injector1.CreateFrom<TConcrete>(inputParms, properties));
        }

        public static void Register2WrapperNonGeneric<T, TConcrete>(IServiceLoaderBinding<T> binding, string named)
            where TConcrete : T
        {
            binding.To(injector1 => injector1.ResolveByNamed<TConcrete>(named));
        }

        public static void Register3NonGeneric<T>(IServiceLoaderBinding<T> binding, T instance)
        {
            binding.To(instance);
        }
    }
}