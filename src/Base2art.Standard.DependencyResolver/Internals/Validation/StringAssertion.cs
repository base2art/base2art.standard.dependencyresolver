﻿namespace Base2art.ComponentModel.Composition.Validation
{
    using System;

    internal static class StringAssertion
    {
        public static IObjectMatcher<string> IsNotNullEmptyOrWhiteSpace(this IMatcher<string> value)
        {
            value.Validate().IsNotNull();
            if (string.IsNullOrWhiteSpace(value.Value))
            {
                throw new ArgumentNullException(nameof(value));
            }

            return new ObjectMatcher<string>(value.Value);
        }
    }
}