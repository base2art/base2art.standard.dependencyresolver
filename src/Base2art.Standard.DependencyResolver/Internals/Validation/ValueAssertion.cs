﻿namespace Base2art.ComponentModel.Composition.Validation
{
    internal static class ValueAssertion
    {
        public static IMatcher<T> Validate<T>(this T value)
        {
            return new Matcher<T>(value);
        }

        public static IObjectMatcher<string> Validate(this string value)
        {
            return new ObjectMatcher<string>(value);
        }

        public static IMatcher<T> And<T>(this IMatcher<T> value)
            where T : struct
        {
            return new Matcher<T>(value);
        }
    }
}