﻿namespace Base2art.ComponentModel.Composition.Validation
{
    using System.Diagnostics.CodeAnalysis;

    [SuppressMessage("Microsoft.Design", "CA1040:AvoidEmptyInterfaces", Justification = "SjY")]
    internal interface IObjectMatcher<out T> : IMatcher<T>
        where T : class
    {
    }
}