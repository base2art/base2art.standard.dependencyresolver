﻿namespace Base2art.ComponentModel.Composition.Validation
{
    using System;

    internal static class ComparableBaseAssertion
    {
        public static IMatcher<T> HasValue<T>(this IMatcher<T> value) where T : IComparable<T>
        {
            value.Validate().IsNotNull();

            // disable once CompareNonConstrainedGenericWithNull
            if (value.Value == null || value.Value.CompareTo(default(T)) == 0)
            {
                throw new ArgumentNullException("value");
            }

            if (typeof(T) == typeof(string))
            {
                var str = value.Value as string;
                if (string.IsNullOrEmpty(str))
                {
                    throw new ArgumentNullException("value");
                }
            }

            return new Matcher<T>(value.Value);
        }
    }
}