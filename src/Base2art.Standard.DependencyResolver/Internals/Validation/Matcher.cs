﻿namespace Base2art.ComponentModel.Composition.Validation
{
    internal class Matcher<T> : IMatcher<T>
    {
        private readonly IMatcher<T> matcher;

        private readonly T value;

        public Matcher(T value)
        {
            this.value = value;
        }

        public Matcher(IMatcher<T> matcher)
        {
            this.matcher = matcher;
        }

        public T Value
        {
            get
            {
                if (this.matcher != null)
                {
                    return this.matcher.Value;
                }

                return this.value;
            }
        }
    }
}