﻿namespace Base2art.ComponentModel.Composition.Validation
{
    using System;
    using System.Globalization;

    internal static class ComparableAssertion
    {
        public static IObjectMatcher<T> EqualTo<T>(this IObjectMatcher<T> value, T expected)
            where T : class, IComparable
        {
            value.Validate().IsNotNull();
            value.Value.Validate().IsNotNull();
            expected.Validate().IsNotNull();

            if (value.Value.CompareTo(expected) != 0)
            {
                var message = string.Format(
                                            CultureInfo.InvariantCulture,
                                            "Validation Exception: expected to be: '{0}'",
                                            expected);

                throw new ArgumentOutOfRangeException("value", message);
            }

            return new ObjectMatcher<T>(value.Value);
        }

        public static IObjectMatcher<T> NotEqualTo<T>(this IObjectMatcher<T> value, T expected)
            where T : class, IComparable
        {
            value.Validate().IsNotNull();
            value.Value.Validate().IsNotNull();
            expected.Validate().IsNotNull();

            if (value.Value.CompareTo(expected) == 0)
            {
                var message = string.Format(CultureInfo.InvariantCulture, "Validation Exception: expected to be: '{0}'", expected);
                throw new ArgumentOutOfRangeException("value", message);
            }

            return new ObjectMatcher<T>(value.Value);
        }

        public static IObjectMatcher<T> Is<T>(this IObjectMatcher<T> value, T expected)
            where T : class, IComparable
        {
            return EqualTo(value, expected);
        }

        public static IObjectMatcher<T> IsNot<T>(this IObjectMatcher<T> value, T expected)
            where T : class, IComparable
        {
            return NotEqualTo(value, expected);
        }
    }
}