﻿namespace Base2art.ComponentModel.Composition.Validation
{
    using System;
    using System.Globalization;

    internal static class ComparableValueAssertion
    {
        public static IMatcher<T> EqualTo<T>(this IMatcher<T> value, T expected)
            where T : struct, IComparable<T>
        {
            value.Validate().IsNotNull();
            if (value.Value.CompareTo(expected) != 0)
            {
                var message = string.Format(CultureInfo.InvariantCulture, "Validation Exception: expected to be: '{0}'", expected);
                throw new ArgumentOutOfRangeException("value", message);
            }

            return new Matcher<T>(value);
        }

        public static IMatcher<T> NotEqualTo<T>(this IMatcher<T> value, T expected)
            where T : struct, IComparable<T>
        {
            value.Validate().IsNotNull();
            if (value.Value.CompareTo(expected) == 0)
            {
                var message = string.Format(CultureInfo.InvariantCulture, "Validation Exception: expected to be: '{0}'", expected);
                throw new ArgumentOutOfRangeException("value", message);
            }

            return new Matcher<T>(value);
        }

        public static IMatcher<T> Is<T>(this IMatcher<T> value, T expected)
            where T : struct, IComparable<T>
        {
            return EqualTo(value, expected);
        }

        public static IMatcher<T> IsNot<T>(this IMatcher<T> value, T expected)
            where T : struct, IComparable<T>
        {
            return NotEqualTo(value, expected);
        }

        public static IMatcher<T> IsNotDefault<T>(this IMatcher<T> value)
            where T : struct, IComparable<T>
        {
            return NotEqualTo(value, default(T));
        }

        public static IMatcher<T> GreaterThan<T>(this IMatcher<T> value, T expected)
            where T : struct, IComparable<T>
        {
            value.Validate().IsNotNull();
            if (value.Value.CompareTo(expected) <= 0)
            {
                var message = string.Format(CultureInfo.InvariantCulture, "Validation Exception: expected to be Greater Than: '{0}'", expected);
                throw new ArgumentOutOfRangeException("value", message);
            }

            return new Matcher<T>(value);
        }

        public static IMatcher<T> LessThan<T>(this IMatcher<T> value, T expected)
            where T : struct, IComparable<T>
        {
            value.Validate().IsNotNull();
            if (value.Value.CompareTo(expected) >= 0)
            {
                var str = string.Format(CultureInfo.InvariantCulture, "Validation Exception: expected to be Less Than: '{0}'", expected);
                throw new ArgumentOutOfRangeException("value", str);
            }

            return new Matcher<T>(value);
        }

        public static IMatcher<T> GreaterThanOrEqualTo<T>(this IMatcher<T> value, T expected)
            where T : struct, IComparable<T>
        {
            value.Validate().IsNotNull();
            if (value.Value.CompareTo(expected) < 0)
            {
                var message = string.Format(CultureInfo.InvariantCulture, "Validation Exception: expected to be Greater Than or equal to: '{0}'",
                                            expected);
                throw new ArgumentOutOfRangeException("value", message);
            }

            return new Matcher<T>(value);
        }

        public static IMatcher<T> LessThanOrEqualTo<T>(this IMatcher<T> value, T expected)
            where T : struct, IComparable<T>
        {
            value.Validate().IsNotNull();
            if (value.Value.CompareTo(expected) > 0)
            {
                var str = string.Format(CultureInfo.InvariantCulture, "Validation Exception: expected to be Less Than or equal to: '{0}'", expected);
                throw new ArgumentOutOfRangeException("value", str);
            }

            return new Matcher<T>(value);
        }
    }
}