﻿namespace Base2art.ComponentModel.Composition.Validation
{
    internal interface IMatcher<out T>
    {
        T Value { get; }
    }
}