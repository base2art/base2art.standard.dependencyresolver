﻿namespace Base2art.ComponentModel.Composition.Validation
{
    using System;

    internal static class ObjectAssertion
    {
        public static IObjectMatcher<T> And<T>(this IObjectMatcher<T> value)
            where T : class
        {
            return new ObjectMatcher<T>(value);
        }

        public static IMatcher<TPropResult> And<T, TPropResult>(this IObjectMatcher<T> value, Func<T, TPropResult> selector)
            where T : class
        {
            value.Validate().IsNotNull();
            selector.Validate().IsNotNull();
            return new Matcher<TPropResult>(selector(value.Value));
        }

        public static IObjectMatcher<T> IsNotNull<T>(this IMatcher<T> value)
            where T : class
        {
            if (value.Value == null)
            {
                throw new ArgumentNullException("value");
            }

            return new ObjectMatcher<T>(value.Value);
        }

        public static IObjectMatcher<TVerify> IsA<TVerify>(this IMatcher<object> value)
            where TVerify : class
        {
            value.Validate().IsNotNull();
            if (value.Value == null)
            {
                throw new ArgumentNullException("value");
            }

            var verifyValue = value.Value as TVerify;
            if (verifyValue != null)
            {
                return new ObjectMatcher<TVerify>(verifyValue);
            }

            throw new ArgumentException("Type Validation Failed'" + typeof(TVerify) + "'", "value");
        }
    }
}