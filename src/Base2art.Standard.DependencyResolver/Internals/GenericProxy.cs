﻿namespace Base2art.ComponentModel.Composition
{
    using System;
    using System.Linq;
    using System.Reflection;

    internal static class GenericProxy
    {
        public static object CallStatic(
            this Type target,
            Type[] genericTypes,
            string methodName,
            Type[] parameterTypes,
            params object[] objs)
        {
            var method2 = target.GetRuntimeMethods().Single(x => x.Name == methodName);
            var generic2 = method2.MakeGenericMethod(genericTypes);

            return generic2.Invoke(null, objs);
        }
    }
}