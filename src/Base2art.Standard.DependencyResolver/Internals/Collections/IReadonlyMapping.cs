﻿namespace Base2art.ComponentModel.Composition.Collections
{
    using System.Collections.Generic;

    internal interface IReadOnlyMapping<TKey, out TValue>
    {
        int Count { get; }

        IEnumerable<TKey> Keys { get; }

        IEnumerable<TValue> Values { get; }

        bool Contains(TKey key);
    }
}