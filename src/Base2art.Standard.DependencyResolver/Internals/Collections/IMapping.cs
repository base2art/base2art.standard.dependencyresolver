﻿namespace Base2art.ComponentModel.Composition.Collections
{
    internal interface IMapping<in TKey, in TValue>
    {
        void Add(TKey key, TValue value);

        bool Remove(TKey key);

        void Clear();
    }
}