﻿namespace Base2art.ComponentModel.Composition.Collections
{
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;
    using System.Linq;

    [SuppressMessage("Microsoft.Naming", "CA1710:IdentifiersShouldHaveCorrectSuffix", Justification = "SjY")]
    [SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Multi", Justification = "SjY")]
    internal interface IReadOnlyMultiMap<TKey, out TValue> : IReadOnlyMapping<TKey, TValue>, IEnumerable<IGrouping<TKey, TValue>>
    {
        IEnumerable<TValue> this[TKey key] { get; }
    }
}