﻿namespace Base2art.ComponentModel.Composition.Collections
{
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;

    [SuppressMessage("Microsoft.Naming", "CA1710:IdentifiersShouldHaveCorrectSuffix", Justification = "SjY")]
    [SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Multi", Justification = "SjY")]
    internal interface IMultiMap<TKey, TValue> : IReadOnlyMultiMap<TKey, TValue>, IMapping<TKey, TValue>
    {
        new IEnumerable<TValue> this[TKey key] { get; set; }
    }
}