﻿namespace Base2art.ComponentModel.Composition
{
    using System.Collections.Generic;

    internal class HasResolvedContainer
    {
        public HashSet<int> RequestedTypes { get; } = new HashSet<int>();

        public HashSet<int> ResolvedTypes { get; } = new HashSet<int>();
    }
}