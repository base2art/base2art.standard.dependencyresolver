﻿namespace Base2art.ComponentModel.Composition
{
    using System;
    using System.Linq;
    using Validation;

    internal abstract class ServiceLoaderInjectorBase : IServiceLoaderInjector
    {
        public object Resolve(Type contractType)
        {
            return this.Resolve(contractType, false);
        }

        public T Resolve<T>()
        {
            return this.Resolve<T>(false);
        }

        public object ResolveByNamed(Type contractType, string name)
        {
            return this.ResolveByNamed(contractType, name, false);
        }

        public T ResolveByNamed<T>(string name)
        {
            return this.ResolveByNamed<T>(name, false);
        }

        public object[] ResolveAll(Type contractType)
        {
            return this.ResolveAll(contractType, false);
        }

        public T[] ResolveAll<T>()
        {
            return this.ResolveAll<T>(false);
        }

        public T Resolve<T>(bool returnDefaultOnNotFound)
        {
            return (T) this.Resolve(typeof(T), returnDefaultOnNotFound);
        }

        public T ResolveByNamed<T>(string name, bool returnDefaultOnNotFound)
        {
            return (T) this.ResolveByNamed(typeof(T), name, returnDefaultOnNotFound);
        }

        public T[] ResolveAll<T>(bool returnDefaultOnNotFound)
        {
            return this.ResolveAll(typeof(T), returnDefaultOnNotFound).Cast<T>().ToArray();
        }

        public object Resolve(Type contractType, bool returnDefaultOnNotFound)
        {
            contractType.Validate().IsNotNull();

            if (returnDefaultOnNotFound && !this.HasType(contractType))
            {
                return null;
            }

            return this.ResolveInternal(contractType, returnDefaultOnNotFound);
        }

        public object ResolveByNamed(Type contractType, string name, bool returnDefaultOnNotFound)
        {
            contractType.Validate().IsNotNull();
            name.Validate().IsNotNullEmptyOrWhiteSpace();

            if (returnDefaultOnNotFound && !this.HasType(contractType))
            {
                return null;
            }

            return this.ResolveByNamedInternal(contractType, name, returnDefaultOnNotFound);
        }

        public object[] ResolveAll(Type contractType, bool returnDefaultOnNotFound)
        {
            contractType.Validate().IsNotNull();

            if (returnDefaultOnNotFound && !this.HasType(contractType))
            {
                return (object[]) Array.CreateInstance(contractType, 0);
            }

            return this.ResolveAllInternal(contractType, returnDefaultOnNotFound);
        }

        protected abstract object ResolveInternal(Type contractType, bool returnDefaultOnNotFound);

        protected abstract object ResolveByNamedInternal(Type contractType, string name, bool returnDefaultOnNotFound);

        protected abstract object[] ResolveAllInternal(Type contractType, bool returnDefaultOnNotFound);

        protected abstract bool HasType(Type contractType);
    }
}