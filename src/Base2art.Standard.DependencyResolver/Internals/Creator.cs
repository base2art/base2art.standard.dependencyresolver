﻿namespace Base2art.ComponentModel.Composition
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Globalization;
    using System.Linq;
    using System.Reflection;

    internal static class Creator
    {
        public static Array GetServicesArray(this IServiceLoaderInjector resolver, Type type)
        {
            var optionsObjs = resolver.ResolveAll(type, true).ToArray();
            var newArray = Array.CreateInstance(type, optionsObjs.Length);
            Array.Copy(optionsObjs, newArray, optionsObjs.Length);
            return newArray;
        }

        public static bool IsNullableValueType(this Type type)
        {
            return type.GetTypeInfo().IsValueType && type.GetTypeInfo().IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>);
        }

        public static object CreateFromInternal(
            this Type t,
            IServiceLoaderInjector config,
            Func<ParameterInfo, bool> hasParam,
            Func<ParameterInfo, object> getParam)
        {
            var ctors = t.GetTypeInfo().DeclaredConstructors.ToArray();
            if (ctors.Length == 0)
            {
                return Activator.CreateInstance(t);
            }

            if (ctors.Length == 1)
            {
                try
                {
                    return CreateWithCtor(t, config, hasParam, getParam, ctors[0]);
                }
                catch (BindingException ex)
                {
                    throw new BindingException("No Constructors Found", ex);
                }
            }

            var items = GetConstructors(ctors, hasParam, getParam);

            Exception lastException = null;
            foreach (var ctor in items)
            {
                try
                {
                    return CreateWithCtor(t, config, hasParam, getParam, ctor);
                }
                catch (BindingException ex)
                {
                    lastException = ex;
                }
            }

            throw new BindingException("No Constructors Found", lastException);
        }

        private static IEnumerable<ConstructorInfo> GetConstructors(
            ConstructorInfo[] ctors,
            Func<ParameterInfo, bool> hasParam,
            Func<ParameterInfo, object> getParam)
        {
            HashSet<string> requiredParameters = new HashSet<string>();

            var ctor = ctors.First();
            foreach (var parm in ctor.GetParameters())
            {
                if (ctors.All(x => x.GetParameters().Any(a => a.Name == parm.Name)))
                {
                    requiredParameters.Add(parm.Name);
                }
            }

            var hasMetAll = new List<Tuple<int, ConstructorInfo>>();

            foreach (var constructorInfo in ctors)
            {
                HashSet<string> optionalParameters = new HashSet<string>();
                var hasMetAllLocal = true;
                foreach (var parameter in constructorInfo.GetParameters())
                {
                    if (!requiredParameters.Contains(parameter.Name))
                    {
                        optionalParameters.Add(parameter.Name);
                        if (!hasParam(parameter))
                        {
                            hasMetAllLocal = false;
                        }
                    }
                }

                if (hasMetAllLocal)
                {
                    hasMetAll.Add(Tuple.Create(optionalParameters.Count, constructorInfo));
                }
            }

            return (from x in hasMetAll
                    orderby x.Item1 descending
                    select x.Item2).ToList();
        }

        private static object CreateWithCtor(
            Type t,
            IServiceLoaderInjector config,
            Func<ParameterInfo, bool> hasParam,
            Func<ParameterInfo, object> getParam,
            ConstructorInfo ctor)
        {
            var parms = ctor.GetParameters();
            var length = parms.Length;
            if (length == 0)
            {
                return Activator.CreateInstance(t);
            }

            var obj = new object[length];
            for (var i = 0; i < obj.Length; i++)
            {
                var parameterInfo = parms[i];
                if (hasParam(parameterInfo))
                {
                    obj[i] = getParam(parameterInfo);
                }
                else
                {
                    if (parameterInfo.ParameterType == typeof(string))
                    {
                        throw new BindingException($"{t.FullName} missing {parameterInfo.Name}");
                    }

                    if (parameterInfo.ParameterType.GetTypeInfo().IsPrimitive)
                    {
                        if (!parameterInfo.HasDefaultValue)
                        {
                            throw new BindingException($"{t.FullName} missing {parameterInfo.Name}");
                        }

                        obj[i] = parameterInfo.DefaultValue;
                    }
                    else if (parameterInfo.ParameterType.IsNullableValueType())
                    {
                        if (!parameterInfo.HasDefaultValue)
                        {
                            throw new BindingException($"{t.FullName} missing {parameterInfo.Name}");
                        }

                        obj[i] = parameterInfo.DefaultValue;
                    }
                    else
                    {
                        var parmType = parameterInfo.ParameterType;
                        if (parmType.IsArray)
                        {
                            var innerParmType = parmType.GetElementType();
                            obj[i] = config.GetServicesArray(innerParmType);
                        }
                        else
                        {
                            if (!parameterInfo.HasDefaultValue)
                            {
                                obj[i] = config.Resolve(parmType);
                            }
                            else
                            {
                                try
                                {
                                    obj[i] = config.Resolve(parmType);
                                }
                                catch (KeyNotFoundException e)
                                {
                                    obj[i] = parameterInfo.DefaultValue;
                                }
                            }
                        }
                    }
                }
            }

            return Activator.CreateInstance(t, obj);
        }

        public static void SetProperties(IDictionary<string, object> properties, object obj)
        {
            if (properties == null)
            {
                return;
            }

            if (properties.Count > 0)
            {
                var objType = obj.GetType();
                foreach (var element in properties)
                {
                    var prop = objType.GetRuntimeProperty(element.Key);
                    var propSet = prop?.SetMethod;

                    propSet?.Invoke(obj, new[] {Convert(properties, () => prop.Name, () => prop.PropertyType)});
                }
            }
        }

        public static object Convert(IDictionary<string, object> inputParms, Func<string> nameGetter, Func<Type> typeGetter)
        {
            var value = inputParms[nameGetter()];
            var type = typeGetter();

            if (value == null)
            {
                return null;
            }

            var type2 = value.GetType();

            if (type2 == type)
            {
                return value;
            }

            if (type.GetTypeInfo().IsAssignableFrom(type2.GetTypeInfo()))
            {
                return value;
            }

            var converter = TypeDescriptor.GetConverter(type);

            if (converter.CanConvertFrom(type2))
            {
                return converter.ConvertFrom(value);
            }

            var message = string.Format(CultureInfo.InvariantCulture, "Cannot convert from '{0}' to '{1}'", type2, type);
            throw new FormatException(message);
        }
    }
}