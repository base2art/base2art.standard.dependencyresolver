﻿namespace Base2art.ComponentModel.Composition
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    ///     A set of methods for creating and manipulating the injector.
    /// </summary>
    public static class ServiceLoader
    {
        /// <summary>
        ///     Creates a loader.
        /// </summary>
        /// <returns>The new isolated loader.</returns>
        public static IBindableAndSealableServiceLoaderInjector CreateLoader()
        {
            return new ServiceLoaderInjector();
        }

        /// <summary>
        ///     Register an instance with with reflection of all interfaces as a binding.
        /// </summary>
        /// <param name="injector">The injector to register the instance to.</param>
        /// <param name="instance">The isntance to register.</param>
        /// <param name="registerImplementedInterfaces">A value indicating if all interfaces of the instance are registered.</param>
        /// <typeparam name="T">The type of instance to register.</typeparam>
        public static void RegisterInstance<T>(
            this IBindableAndSealableServiceLoaderInjector injector,
            T instance,
            bool registerImplementedInterfaces)
        {
            var items = new List<Tuple<Type, object>>();
            items.Add(Tuple.Create(typeof(T), (object) instance));

            if (registerImplementedInterfaces)
            {
                foreach (var item in DynamicRegistrarInternal.GetAllInterfacesImplementedBy<T>())
                {
                    items.Add(Tuple.Create(item, (object) instance));
                }
            }

            foreach (var item in items)
            {
                var parm1 = ServiceLoaderBindingType.Singleton;
                var rez = typeof(DynamicRegistrarInternal).CallStatic(
                                                                      new[] {item.Item1},
                                                                      nameof(DynamicRegistrarInternal.Register1NonGeneric),
                                                                      DynamicRegistrarInternal.Register1NonGenericParameterTypes, injector, parm1,
                                                                      null);
                typeof(DynamicRegistrarInternal).CallStatic(
                                                            new[] {item.Item1},
                                                            nameof(DynamicRegistrarInternal.Register3NonGeneric),
                                                            DynamicRegistrarInternal.Register3NonGenericParameterTypes, rez, item.Item2);
            }
        }

        /// <summary>
        ///     A method used to register a type dynamically, based on types/
        /// </summary>
        /// <param name="injector">The injector to register the instance to.</param>
        /// <param name="requested">The requestable type.</param>
        /// <param name="fulfilling">The type that fulfills the request.</param>
        /// <param name="parameters">The parameters that helps to satisfy the fulfilling type.</param>
        /// <param name="properties">The properties that helps to satisfy the fulfilling type.</param>
        /// <param name="isSingleton">A value indicating that this item should be bould as a singleton.</param>
        public static void Register(
            this IBindableServiceLoaderInjector injector,
            Type requested,
            Type fulfilling,
            IDictionary<string, object> parameters,
            IDictionary<string, object> properties,
            bool isSingleton)
        {
            if (requested != fulfilling)
            {
                var named = Guid.NewGuid().ToString("N");
                // Implementation
                {
                    var parm1 = isSingleton ? ServiceLoaderBindingType.Singleton : ServiceLoaderBindingType.Instance;
                    var item = typeof(DynamicRegistrarInternal).CallStatic(
                                                                           new[] {fulfilling},
                                                                           nameof(DynamicRegistrarInternal.Register1NonGeneric),
                                                                           DynamicRegistrarInternal.Register1NonGenericParameterTypes, injector,
                                                                           parm1, named);

                    Func<IDictionary<string, object>, IDictionary<string, object>> c = x => x ?? new Dictionary<string, object>();
                    typeof(DynamicRegistrarInternal).CallStatic(
                                                                new[] {fulfilling, fulfilling},
                                                                nameof(DynamicRegistrarInternal.Register2NonGeneric),
                                                                DynamicRegistrarInternal.Register2NonGenericParameterTypes, item, c(parameters),
                                                                c(properties));
                }

                // interface
                {
                    var parm1 = isSingleton ? ServiceLoaderBindingType.Singleton : ServiceLoaderBindingType.Instance;
                    var item = typeof(DynamicRegistrarInternal).CallStatic(
                                                                           new[] {requested},
                                                                           nameof(DynamicRegistrarInternal.Register1NonGeneric),
                                                                           DynamicRegistrarInternal.Register1NonGenericParameterTypes, injector,
                                                                           parm1, null);

                    Func<IDictionary<string, object>, IDictionary<string, object>> c = x => x ?? new Dictionary<string, object>();
                    typeof(DynamicRegistrarInternal).CallStatic(
                                                                new[] {requested, fulfilling},
                                                                nameof(DynamicRegistrarInternal.Register2WrapperNonGeneric),
                                                                DynamicRegistrarInternal.Register2NonGenericParameterTypes, item, named);
                }
            }
            else
            {
                var parm1 = isSingleton ? ServiceLoaderBindingType.Singleton : ServiceLoaderBindingType.Instance;
                var item = typeof(DynamicRegistrarInternal).CallStatic(
                                                                       new[] {fulfilling},
                                                                       nameof(DynamicRegistrarInternal.Register1NonGeneric),
                                                                       DynamicRegistrarInternal.Register1NonGenericParameterTypes, injector, parm1,
                                                                       null);

                Func<IDictionary<string, object>, IDictionary<string, object>> c = x => x ?? new Dictionary<string, object>();
                typeof(DynamicRegistrarInternal).CallStatic(
                                                            new[] {fulfilling, fulfilling},
                                                            nameof(DynamicRegistrarInternal.Register2NonGeneric),
                                                            DynamicRegistrarInternal.Register2NonGenericParameterTypes, item, c(parameters),
                                                            c(properties));
            }
        }
    }
}