﻿namespace Base2art.ComponentModel.Composition
{
    /// <summary>
    ///     A contract implemented by all modules for sub registrations.
    /// </summary>
    public interface IServiceLoaderInjectorModule
    {
        /// <summary>
        ///     The primary method used to register the module.
        /// </summary>
        /// <param name="serviceLoaderInjector">The injector.</param>
        void Configure(IBindableServiceLoaderInjector serviceLoaderInjector);
    }
}