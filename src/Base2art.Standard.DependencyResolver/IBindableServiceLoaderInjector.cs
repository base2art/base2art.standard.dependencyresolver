﻿namespace Base2art.ComponentModel.Composition
{
    /// <summary>
    ///     A contract that allows users to register bindings.
    /// </summary>
    public interface IBindableServiceLoaderInjector : IServiceLoaderInjector
    {
        /// <summary>
        ///     A method that allows users begin the binding process.
        /// </summary>
        /// <typeparam name="T">The type to bind.</typeparam>
        /// <returns></returns>
        IServiceLoaderBinding<T> Bind<T>();

        /// <summary>
        ///     A method that gives consumers the ability to bind whole modules full of registrations.
        /// </summary>
        /// <param name="module">The module to register.</param>
        /// <returns>The current binder.</returns>
        IBindableServiceLoaderInjector BindModule(IServiceLoaderInjectorModule module);

        /// <summary>
        ///     A method that gives consumers the ability to bind whole modules full of registrations.
        /// </summary>
        /// <typeparam name="TModule">The type of module to bind.</typeparam>
        /// <returns>The current binder.</returns>
        IBindableServiceLoaderInjector BindModule<TModule>()
            where TModule : IServiceLoaderInjectorModule, new();
    }
}