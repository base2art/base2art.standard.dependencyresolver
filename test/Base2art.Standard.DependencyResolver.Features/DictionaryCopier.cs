﻿namespace Base2art.ComponentModel.Composition
{
    using System.Collections.Generic;

    public static class DictionaryCopier
    {
        public static Dictionary<TKey, TValue> Copy<TKey, TValue>(this Dictionary<TKey, TValue> values)
        {
            var dict = new Dictionary<TKey, TValue>();

            foreach (var pair in values)
            {
                dict.Add(pair.Key, pair.Value);
            }

            return dict;
        }

        public static IDictionary<TKey, TValue> Copy<TKey, TValue>(this IDictionary<TKey, TValue> values)
        {
            var dict = new Dictionary<TKey, TValue>();

            foreach (var pair in values)
            {
                dict.Add(pair.Key, pair.Value);
            }

            return dict;
        }
    }
}