namespace Base2art.ComponentModel.Composition
{
    using System;
    using System.Collections.Generic;
    using Fixtures;
    using Fixtures.NestingOfTypes;
    using FluentAssertions;
    using Xunit;

    public class RealWorldFeature
    {
        [Fact]
        public void Load()
        {
            var loader = ServiceLoader.CreateLoader();

            loader.Bind<Interface1>().As(ServiceLoaderBindingType.Singleton).ToInstanceCreator<KlassL1>();

            loader.Bind<InterfaceA>().ToInstanceCreator<TypeA>();
            loader.Bind<InterfaceB>().ToInstanceCreator<TypeB>();

            loader.Resolve<InterfaceB>().Should().NotBeNull();
            loader.Resolve<InterfaceB>().Should().NotBeNull();
//            loader.Resolve<InterfaceB>().Should().NotBeNull();
        }

        [Fact]
        public void ShouldResolveNoInheritance()
        {
            var loader = ServiceLoader.CreateLoader();
            Interface1 interfaceOf = new KlassL1();
            loader.Register(
                            typeof(InterfaceA),
                            typeof(TypeA),
                            new Dictionary<string, object>
                            {
                                {"it1", interfaceOf}
                            },
                            new Dictionary<string, object>(),
                            false);

            loader.Resolve<InterfaceA>().Should().NotBeNull();
        }
        
        [Fact]
        public void ShouldResolveInheritance()
        {
            var loader = ServiceLoader.CreateLoader();
            var interfaceOf = new KlassL1();
            loader.Register(
                            typeof(InterfaceA),
                            typeof(TypeA),
                            new Dictionary<string, object>
                            {
                                {"it1", interfaceOf}
                            },
                            new Dictionary<string, object>(),
                            false);

            loader.Resolve<InterfaceA>().Should().NotBeNull();
        }

        [Fact]
        public void LoadCreateWithProperties()
        {
            var loader = ServiceLoader.CreateLoader();

            loader.Register(
                            typeof(InterfaceWithProperties),
                            typeof(ClassWithProperties),
                            null,
                            null,
                            false);

            var properties = new Dictionary<string, object>
                             {
                                 {"TEST", 1}
                             };
            loader.CreateFrom<ClassWithProperties>(new Dictionary<string, object>(), properties)
                  .Should().NotBeNull();
//            loader.Resolve<InterfaceB>().Should().NotBeNull();
        }

        private interface InterfaceA
        {
        }

        private class TypeA : InterfaceA
        {
            static TypeA()
            {
                Console.WriteLine("");
            }
            
            public TypeA(Interface1 it1)
            {
            }
        }

        private interface InterfaceB
        {
        }

        private class TypeB : InterfaceB
        {
            public TypeB(Interface1 it1, InterfaceA ita)
            {
            }
        }
    }
}