﻿namespace Base2art.ComponentModel.Composition
{
    using System;
    using System.Collections.Generic;
    using Fixtures;
    using Fixtures.NestingOfTypes;
    using Fixtures.SpecificExample;
    using FluentAssertions;
    using Xunit;

    public class ServiceLoaderFeature
    {
        public class CustomModule : IServiceLoaderInjectorModule
        {
            public void Configure(IBindableServiceLoaderInjector serviceLoaderInjector)
            {
                serviceLoaderInjector.Bind<ITestFactory>().To(new TestFactory(1));
            }
        }

        private class A
        {
            public A(B b)
            {
            }
        }

        private class B
        {
            public B(A a)
            {
            }
        }

        [Fact]
        public void Bug_WithServiceLoaderAndSingleton()
        {
            {
                var loader = ServiceLoader.CreateLoader();
                loader.Bind<Interface1>().As(ServiceLoaderBindingType.Singleton).ToInstanceCreator<KlassL1>();
                loader.Bind<Interface2>().ToInstanceCreator<KlassL2>();
                loader.Bind<Interface3>().ToInstanceCreator<KlassL3>();

                loader.Resolve<Interface3>().Should().NotBeNull();
            }

            {
                var loader = ServiceLoader.CreateLoader();
                loader.Bind<Interface1>().ToInstanceCreator<KlassL1>();
                loader.Bind<Interface2>().As(ServiceLoaderBindingType.Singleton).ToInstanceCreator<KlassL2>();
                loader.Bind<Interface3>().ToInstanceCreator<KlassL3>();

                loader.Resolve<Interface3>().Should().NotBeNull();
            }

            {
                var loader = ServiceLoader.CreateLoader();
                loader.Bind<Interface1>().ToInstanceCreator<KlassL1>();
                loader.Bind<Interface2>().ToInstanceCreator<KlassL2>();
                loader.Bind<Interface3>().As(ServiceLoaderBindingType.Singleton).ToInstanceCreator<KlassL3>();

                loader.Resolve<Interface3>().Should().NotBeNull();
            }
        }

        [Fact]
        public void Should_FailToResolve()
        {
            var injector = ServiceLoader.CreateLoader();
            Action mut = () => injector.Resolve<ITestFactory>();
            mut.Should().Throw<KeyNotFoundException>().And.Message.Should().Contain("ITestFactory");
        }

        [Fact]
        public void Should_FailToResolve_Nested()
        {
            var injector = ServiceLoader.CreateLoader();
            injector.Bind<Interface2>().ToInstanceCreator<KlassL2>();
            Action mut = () => injector.Resolve<Interface2>();
            mut.Should().Throw<KeyNotFoundException>().And.Message.Should().Contain("Interface1");
        }

        [Fact]
        public void ShouldBindModule_Generic()
        {
            var injector = ServiceLoader.CreateLoader();
            injector.BindModule<CustomModule>();
            injector.Resolve<ITestFactory>().GetInstanceNumber().Should().Be(1);
        }

        [Fact]
        public void ShouldBindModule_Generic_Sealed()
        {
            var injector = ServiceLoader.CreateLoader();
            injector.Seal();
            Action mut = () => injector.BindModule<CustomModule>();
            mut.Should().Throw<NotSupportedException>();
        }

        [Fact]
        public void ShouldBindModule_NonGeneric()
        {
            var injector = ServiceLoader.CreateLoader();
            injector.BindModule(new CustomModule());
            injector.Resolve<ITestFactory>().GetInstanceNumber().Should().BeGreaterOrEqualTo(1);
        }

        [Fact]
        public void ShouldBindModule_NonGeneric_NullRef()
        {
            var injector = ServiceLoader.CreateLoader();
            Action mut = () => injector.BindModule(null);
            mut.Should().Throw<ArgumentNullException>();
        }

        /*
        [Fact]
        public void ShouldSerializeAndDeserialize()
        {
            var m = new CircularDependencyException();
            var ser = new BinaryItemSerializer<CircularDependencyException>();
            ser.Deserialize(ser.Serialize(m)).Message.Should().Be(m.Message);

            var m1 = new CircularDependencyException("ABC");
            ser.Clone(m1).Message.Should().Be(m1.Message);

            var m2 = new CircularDependencyException("ABCD", new ApplicationException("1"));
            var m3 = ser.Clone(m2);
            m3.Message.Should().Be(m2.Message);
            m3.InnerException.Message.Should().Be(m3.InnerException.Message);
        }
        */

        [Fact]
        public void ShouldBindModule_NonGeneric_Sealed()
        {
            var injector = ServiceLoader.CreateLoader();
            injector.Seal();
            Action mut = () => injector.BindModule(new CustomModule());
            mut.Should().Throw<NotSupportedException>();
        }

        [Fact]
        public void ShouldCreateWhereMissingParams()
        {
            TestFactory.Reset(45);
            var loader = ServiceLoader.CreateLoader();
            loader.Register(typeof(ITestFactory), typeof(TestFactory), null, null, false);
            loader.Register(typeof(ITestFactoryProxy), typeof(TestFactoryProxy), null, null, false);
            loader.Resolve<ITestFactoryProxy>().GetInstanceNumber().Should().Be(46);
        }

        [Fact]
        public void ShouldGetCircularInstances()
        {
            var loader = ServiceLoader.CreateLoader();
            loader.Bind<IInterface1>().To(x => new Implementation1(x.Resolve<IInterface2>()));
            loader.Bind<IInterface2>().To(x => new Implementation2(x.Resolve<IInterface1>()));
            new Action(() => loader.Resolve<IInterface2>()).Should().Throw<CircularDependencyException>();
        }

        [Fact]
        public void ShouldGetInstanceByName()
        {
            var loader = ServiceLoader.CreateLoader();
            loader.Bind<ITestFactory>().Named("TF").To(new TestFactory());

            loader.Bind<ITestFactory>()
                  .Named("ATF")
                  .As(ServiceLoaderBindingType.Instance)
                  .To(x => new AlternateTestFactory());

            loader.Seal();

            new Action(() => loader.ResolveByNamed<ITestFactory>("ABF")).Should().Throw<KeyNotFoundException>();
            new Action(() => loader.ResolveByNamed(typeof(ITestFactory), "ABF")).Should().Throw<KeyNotFoundException>();

            var value = loader.ResolveByNamed<ITestFactory>("ATF");
            value.Should().BeOfType<AlternateTestFactory>();
            value.GetInstanceNumber().Should().Be(42);

            var valueByType = loader.ResolveByNamed(typeof(ITestFactory), "ATF");
            valueByType.Should().BeOfType<AlternateTestFactory>();
            ((ITestFactory) valueByType).GetInstanceNumber().Should().Be(42);

            loader.VerifyAll();
        }

        [Fact]
        public void ShouldGetInstanceByNameForClass()
        {
            var loader = ServiceLoader.CreateLoader();
            loader.Bind<ITestFactory>().Named("TF").To(new TestFactory());

            loader.Bind<ITestFactory>()
                  .Named("ATF")
                  .As(ServiceLoaderBindingType.Instance)
                  .To(x => new AlternateTestFactory());

            loader.Seal();

//            var klazz = typeof(ITestFactory).GetClass().As<>();
            new Action(() => loader.ResolveByNamed<ITestFactory>("ABF")).Should().Throw<KeyNotFoundException>();

            var value = loader.ResolveByNamed<ITestFactory>("ATF");
            value.Should().BeOfType<AlternateTestFactory>();

            value.GetInstanceNumber().Should().Be(42);

            loader.VerifyAll();
        }

        [Fact]
        public void ShouldGetInstanceNotThrowExceptionWhenTypeNotRegisteredAndSpecifiyNoException()
        {
            var loader = ServiceLoader.CreateLoader();

            loader.Resolve<ITestFactory>(true).Should().BeNull();
        }

        [Fact]
        public void ShouldGetInstancesNotThrowExceptionWhenTypeNotRegisteredAndSpecifiyNoException()
        {
            var loader = ServiceLoader.CreateLoader();
            loader.ResolveAll<ITestFactory>(true).Should().Equal();
        }

        [Fact]
        public void ShouldGetInstancesThrowExceptionWhenTypeNotRegistered()
        {
            var loader = ServiceLoader.CreateLoader();

            new Action(() => loader.ResolveAll<ITestFactory>()).Should().Throw<KeyNotFoundException>();
        }

        [Fact]
        public void ShouldGetInstanceThrowExceptionWhenTypeNotRegistered()
        {
            var loader = ServiceLoader.CreateLoader();

            new Action(() => loader.Resolve<ITestFactory>()).Should().Throw<KeyNotFoundException>();
        }

        [Fact]
        public void ShouldGetNamedInstanceNotThrowExceptionWhenTypeNotRegisteredAndSpecifiyNoException()
        {
            var loader = ServiceLoader.CreateLoader();
            loader.Bind<ITestFactory>().Named("Scott").To(x => new TestFactory(9));

            loader.ResolveByNamed<ITestFactory>("Scott", true).GetInstanceNumber().Should().Be(9);
            loader.ResolveByNamed<ITestFactory>("Matt", true).Should().BeNull();

            loader.ResolveByNamed<ITestFactory>("Matt", true).Should().BeNull();
        }

        /*
        [Fact]
        public void ShouldHaveStaticApi()
        {
            new Action(() => ServiceLoader.GetInstance<ITestFactory>()).Should().Throw<InvalidOperationException>();
            new Action(() => ServiceLoader.GetInstanceNamed<ITestFactory>("abc")).Should().Throw<InvalidOperationException>();
            new Action(() => ServiceLoader.GetInstances<ITestFactory>()).Should().Throw<InvalidOperationException>();

            {
                var mock = new Mock<IServiceLoaderInjector>();
                mock.Setup(x => x.Resolve<ITestFactory>()).Returns(new TestFactory());
                ServiceLoader.PrimaryLoader = mock.Object;
                ServiceLoader.GetInstance<ITestFactory>();
                mock.Verify(x => x.Resolve<ITestFactory>());
            }

            {
                var mock = new Mock<IServiceLoaderInjector>();
                mock.Setup(x => x.ResolveByNamed<ITestFactory>("abc")).Returns(new TestFactory());
                ServiceLoader.PrimaryLoader = mock.Object;
                ServiceLoader.GetInstanceNamed<ITestFactory>("abc");
                mock.Verify(x => x.ResolveByNamed<ITestFactory>("abc"));
            }

            {
                var mock = new Mock<IServiceLoaderInjector>();
                mock.Setup(x => x.ResolveAll<ITestFactory>()).Returns(new ITestFactory[] { new TestFactory() });
                ServiceLoader.PrimaryLoader = mock.Object;
                ServiceLoader.GetInstances<ITestFactory>();
                mock.Verify(x => x.ResolveAll<ITestFactory>());
            }
            
            // Allow true / false
            {
                var mock = new Mock<IServiceLoaderInjector>();
                mock.Setup(x => x.Resolve<ITestFactory>()).Returns(new TestFactory());
                ServiceLoader.PrimaryLoader = mock.Object;
                ServiceLoader.GetInstance<ITestFactory>(true);
                mock.Verify(x => x.Resolve<ITestFactory>(true));
            }

            {
                var mock = new Mock<IServiceLoaderInjector>();
                mock.Setup(x => x.ResolveByNamed<ITestFactory>("abc")).Returns(new TestFactory());
                ServiceLoader.PrimaryLoader = mock.Object;
                ServiceLoader.GetInstanceNamed<ITestFactory>("abc", true);
                mock.Verify(x => x.ResolveByNamed<ITestFactory>("abc", true));
            }

            {
                var mock = new Mock<IServiceLoaderInjector>();
                mock.Setup(x => x.ResolveAll<ITestFactory>()).Returns(new ITestFactory[] { new TestFactory() });
                ServiceLoader.PrimaryLoader = mock.Object;
                ServiceLoader.GetInstances<ITestFactory>(true);
                mock.Verify(x => x.ResolveAll<ITestFactory>(true));
            }
        }
         */
        [Fact]
        public void ShouldGetNestedInstance()
        {
            var loader = ServiceLoader.CreateLoader();
            loader.Bind<ITestFactory>().To(x => new TestFactory());
            loader.Bind<IChildTestFactory>()
                  .To(x => new ChildTestFactory(x.Resolve<ITestFactory>()));
        }

        [Fact]
        public void ShouldGetNestedInstances()
        {
            var loader = ServiceLoader.CreateLoader();
            loader.Bind<ITestFactory>().To(x => new TestFactory(99));
            loader.Bind<ITestFactory>().To(x => new TestFactory(100));
            loader.Bind<IChildTestFactory>()
                  .To(x => new ChildTestFactoryMultiple(x.ResolveAll<ITestFactory>()));
            loader.Resolve<IChildTestFactory>().GetInstanceName().Should().Be("99100");
        }

        [Fact]
        public void ShouldGetNestedNamedInstance()
        {
            var loader = ServiceLoader.CreateLoader();
            loader.Bind<ITestFactory>().Named("s").To(x => new TestFactory(99));
            loader.Bind<ITestFactory>().Named("t").To(x => new TestFactory(100));
            loader.Bind<IChildTestFactory>()
                  .To(x => new ChildTestFactory(x.ResolveByNamed<ITestFactory>("t")));
            loader.Resolve<IChildTestFactory>().GetInstanceName().Should().Be("100");
        }

        [Fact]
        public void ShouldGetWhenNestedNamedInstanceIsNull()
        {
            var loader = ServiceLoader.CreateLoader();
            loader.Bind<ITestFactory>().Named("s").To(x => new TestFactory(99));
            loader.Bind<ITestFactory>().Named("t").To(x => new TestFactory(100));
            loader.Bind<ITestFactoryProxy>()
                  .Named("Alpha")
                  .To(x => new TestFactoryProxy(x.ResolveByNamed<ITestFactory>("NULL", true)));
            loader.Bind<ITestFactoryProxy>()
                  .Named("Beta")
                  .To(x => new TestFactoryProxy(x.ResolveByNamed<ITestFactory>("NULL", false)));
            loader.ResolveByNamed<ITestFactoryProxy>("Alpha").GetInstanceNumber().Should().Be(-85);

            new Action(() => loader.ResolveByNamed<ITestFactoryProxy>("Beta")).Should().Throw<KeyNotFoundException>();
        }

        [Fact]
        public void ShouldGetWithoutCircularInstances()
        {
            var loader = ServiceLoader.CreateLoader();

            loader.Bind<IUserService>().ToInstanceCreator<UserService>();
            loader.Bind<ICacheService>().ToInstanceCreator<CacheService>();
            loader.Bind<PersonService>().ToInstanceCreator<PersonService>();
            loader.Bind<ILogger>().ToInstanceCreator<Logger>();
            new Action(() => loader.Resolve(typeof(PersonService))).Should().NotThrow<CircularDependencyException>();
            new Action(() => loader.Resolve(typeof(PersonService))).Should().NotThrow<CircularDependencyException>();
        }

        [Fact]
        public void ShouldHandleSuperCircularReference()
        {
            var loader = ServiceLoader.CreateLoader();
            loader.Bind<ITestFactory>().To(x => x.Resolve<ITestFactory>());

            new Action(() => loader.Resolve<ITestFactory>()).Should().Throw<CircularDependencyException>();
        }

        [Fact]
        public void ShouldHaveUniqueNameByType()
        {
            var loader = ServiceLoader.CreateLoader();
            loader.Bind<ITestFactory>()
                  .As(ServiceLoaderBindingType.Singleton)
                  .Named("TF")
                  .To(new TestFactory());

            new Action(
                       () =>
                           loader.Bind<ITestFactory>()
                                 .As(ServiceLoaderBindingType.Instance)
                                 .Named("TF")
                                 .To(x => new AlternateTestFactory()))
//                .Should().Throw<DuplicateNameException>();
                .Should().Throw<InvalidOperationException>();
        }

        [Fact]
        public void ShouldLoadTypeByDelegateWithInstance()
        {
            var loader = ServiceLoader.CreateLoader();
            loader.Bind<ITestFactory>()
                  .As(ServiceLoaderBindingType.Instance)
                  .To(x => new TestFactory());

            loader.Resolve<ITestFactory>().GetInstanceNumber()
                  .Should().NotBe(loader.Resolve<ITestFactory>().GetInstanceNumber());
        }

        [Fact]
        public void ShouldLoadTypeByDelegateWithSingleton()
        {
            var loader = ServiceLoader.CreateLoader();
            loader.Bind<ITestFactory>()
                  .As(ServiceLoaderBindingType.Singleton)
                  .To(x => new TestFactory());

            loader.Resolve<ITestFactory>().GetInstanceNumber()
                  .Should().Be(loader.Resolve<ITestFactory>().GetInstanceNumber());

            ((ITestFactory) loader.Resolve(typeof(ITestFactory))).GetInstanceNumber()
                                                                 .Should()
                                                                 .Be(loader.Resolve<ITestFactory>().GetInstanceNumber());
        }

        [Fact]
        public void ShouldLoadTypeByInstance()
        {
            var loader = ServiceLoader.CreateLoader();
            loader.Bind<ITestFactory>().To(new TestFactory());

            loader.Resolve<ITestFactory>().GetInstanceNumber()
                  .Should().Be(loader.Resolve<ITestFactory>().GetInstanceNumber());

            loader.VerifyAll();
        }

        [Fact]
        public void ShouldLoadTypeByInstanceByClass()
        {
            var loader = ServiceLoader.CreateLoader();
            loader.Bind<ITestFactory>().To(new TestFactory());

            loader.Resolve<ITestFactory>().GetInstanceNumber() // typeof().GetClass().As<ITestFactory>()).GetInstanceNumber()
                  .Should().Be(loader.Resolve<ITestFactory>().GetInstanceNumber());

            loader.VerifyAll();
        }

        [Fact]
        public void ShouldLoadTypeByInstanceNestedDependencies()
        {
            var loader = ServiceLoader.CreateLoader();
            loader.Bind<ITestFactoryGrouper>().ToInstanceCreator<TestFactoryGrouper>();
            loader.Bind<ITestFactoryProxy>().ToInstanceCreator<TestFactoryProxy>();
            loader.Bind<ITestFactory>().To(x => new TestFactory(7));

            loader.Resolve<ITestFactoryProxy>().GetInstanceNumber()
                  .Should().Be(loader.Resolve<ITestFactoryProxy>().GetInstanceNumber());

            loader.VerifyAll();
        }

        [Fact]
        public void ShouldLoadTypeByInstanceUsingConcreteCreator()
        {
            var loader = ServiceLoader.CreateLoader();
            loader.Bind<ITestFactory>().ToInstanceCreator<TestFactory>();

            loader.Resolve<ITestFactory>().GetInstanceNumber()
                  .Should().Be(loader.Resolve<ITestFactory>().GetInstanceNumber() - 1);

            loader.VerifyAll();
        }

        [Fact]
        public void ShouldLoadTypeByInstanceUsingConcreteCreatorWithCircularDependency()
        {
            var loader = ServiceLoader.CreateLoader();
            loader.Bind<A>().ToInstanceCreator<A>();
            loader.Bind<B>().ToInstanceCreator<B>();

            Assert.Throws<CircularDependencyException>(() => loader.Resolve<A>());
        }

        [Fact]
        public void ShouldLoadTypeByInstanceUsingConcreteCreatorWithNoDefault()
        {
            var loader = ServiceLoader.CreateLoader();
            loader.Bind<ITestFactoryProxy>().ToInstanceCreator<TestFactoryProxy>();
            loader.Bind<ITestFactory>().To(x => new TestFactory(7));

            loader.Resolve<ITestFactoryProxy>().GetInstanceNumber()
                  .Should().Be(loader.Resolve<ITestFactoryProxy>().GetInstanceNumber());

            loader.VerifyAll();
        }

        [Fact]
        public void ShouldLoadTypeByInstanceUsingConcreteCreatorWithNoDefault_AndProxyByDelegate()
        {
            var loader = ServiceLoader.CreateLoader();

            loader.Bind<ITestFactory>()
                  .As(ServiceLoaderBindingType.Singleton)
                  .To(new TestFactory(7));

            loader.Bind<ITestFactoryProxy>()
                  .As(ServiceLoaderBindingType.Instance)
                  .ToInstanceCreator<TestFactoryProxy>();
//
            //            loader.Resolve<ITestFactoryProxy>().GetInstanceNumber()
            //                .Should().Be(loader.Resolve<ITestFactoryProxy>().GetInstanceNumber());

            loader.VerifyAll();

            loader.Resolve<ITestFactoryProxy>().Should().NotBeNull();
        }

        [Fact]
        public void ShouldLoadTypeByInstanceUsingConcreteCreatorWithZeroCtors()
        {
            var loader = ServiceLoader.CreateLoader();
            loader.Bind<ITestFactory>().ToInstanceCreator<TestFactoryNoCtor>();

            Assert.Throws<MissingMethodException>(() => loader.Resolve<ITestFactory>());
        }

        [Fact]
        public void ShouldLockButAbleToGetInstance()
        {
            var loader = ServiceLoader.CreateLoader();
            loader.Bind<ITestFactory>().To(new TestFactory());

            loader.Seal();

            new Action(() => loader.Bind<ITestFactory>().To(new TestFactory()))
//                .Should().Throw<ReadOnlyException>();
                .Should().Throw<NotSupportedException>();

            loader.Resolve<ITestFactory>().GetInstanceNumber()
                  .Should().Be(loader.Resolve<ITestFactory>().GetInstanceNumber());

            loader.VerifyAll();
        }

        [Fact]
        public void ShouldRegisterMultipleInstances()
        {
            var loader = ServiceLoader.CreateLoader();
            loader.Bind<ITestFactory>().To(new TestFactory());
            loader.Bind<ITestFactory>().To(new AlternateTestFactory());

            loader.Seal();

            var factories = loader.ResolveAll<ITestFactory>();
            factories[0].Should().BeOfType<TestFactory>();
            factories[1].Should().BeOfType<AlternateTestFactory>();

            factories[1].GetInstanceNumber().Should().Be(42);

            loader.VerifyAll();
        }

        [Fact]
        public void ShouldRegisterMultipleInstancesForClass()
        {
            var loader = ServiceLoader.CreateLoader();
            loader.Bind<ITestFactory>().To(new TestFactory());
            loader.Bind<ITestFactory>().To(new AlternateTestFactory());

            loader.Seal();

            var factories = loader.ResolveAll<ITestFactory>();
            factories[0].Should().BeOfType<TestFactory>();
            factories[1].Should().BeOfType<AlternateTestFactory>();

            factories[1].GetInstanceNumber().Should().Be(42);

            loader.VerifyAll();
        }

        [Fact]
        public void ShouldVerifyAllCircular()
        {
            var loader = ServiceLoader.CreateLoader();
            loader.Bind<IInterface1>().To(x => new Implementation1(x.Resolve<IInterface2>()));
            loader.Bind<IInterface2>().To(x => new Implementation2(x.Resolve<IInterface1>()));
            new Action(loader.VerifyAll).Should().Throw<CircularDependencyException>();
        }

//        [Fact]
//        public void Should().ThrowExceptionWhenCallVerifyAllOnChild()
//        {
//            var loader = ServiceLoader.CreateLoader();
//            loader.Bind<IInterface1>().To(x => {x.VerifyAll(); return null;});
//            new Action(() => loader.Resolve<IInterface1>()).Should().Throw<NotImplementedException>();
//        }

        [Fact]
        public void ShouldVerifyAllIncomplete()
        {
            var loader = ServiceLoader.CreateLoader();
            loader.Bind<IInterface2>().To(x => new Implementation2(x.Resolve<IInterface1>()));
            new Action(loader.VerifyAll).Should().Throw<KeyNotFoundException>();
        }

        [Fact]
        public void ShouldVerifyGettingItemByConcreteType()
        {
            var loader = ServiceLoader.CreateLoader();
            loader.Bind<TestFactoryProxy>().To(x => new TestFactoryProxy(x.Resolve<ITestFactory>(true)));
            loader.Resolve<TestFactoryProxy>().GetInstanceNumber().Should().Be(-85);
        }

        [Fact]
        public void ShouldVerifyHasTypeNested()
        {
            var loader = ServiceLoader.CreateLoader();
            loader.Bind<ITestFactory>().To(x => new TestFactory(9));
            loader.Bind<ITestFactoryProxy>().To(x => new TestFactoryProxy(x.Resolve<ITestFactory>()));
            loader.Resolve<ITestFactoryProxy>().GetInstanceNumber().Should().Be(9);
        }

        [Fact]
        public void ShouldVerifyHasTypeNestedReturnsNull()
        {
            var loader = ServiceLoader.CreateLoader();
            loader.Bind<ITestFactoryProxy>().To(x => new TestFactoryProxy(x.Resolve<ITestFactory>(true)));
            loader.Resolve<ITestFactoryProxy>().GetInstanceNumber().Should().Be(-85);
        }

        [Fact]
        public void ShouldVerifySameTypeResolution()
        {
            var loader = ServiceLoader.CreateLoader();
            loader.Register(typeof(ITestFactory), typeof(TestFactory), new Dictionary<string, object> {{"i", 9}}, null, false);
            loader.Register(typeof(ITestFactoryProxy), typeof(TestFactoryProxy), null, null, false);
            loader.Resolve<ITestFactoryProxy>().GetInstanceNumber().Should().Be(9);
            loader.Resolve<TestFactoryProxy>().GetInstanceNumber().Should().Be(9);
        }

        [Fact]
        public void ShouldVerifySameTypeResolutionAsSingleton()
        {
            var loader = ServiceLoader.CreateLoader();
            loader.Register(typeof(ITestFactory), typeof(TestFactory), new Dictionary<string, object> {{"i", 9}}, null, true);
            loader.Resolve<ITestFactory>().GetInstanceNumber().Should().Be(9);

            loader.Register(typeof(ITestFactoryProxy), typeof(TestFactoryProxy), null, null, false);
            loader.Resolve<ITestFactoryProxy>().GetInstanceNumber().Should().Be(9);
            loader.Resolve<TestFactoryProxy>().GetInstanceNumber().Should().Be(9);
        }

        [Fact]
        public void ShouldVerifySameTypeResolutionAsSingleton2()
        {
            var loader = ServiceLoader.CreateLoader();
            loader.Register(typeof(ITestFactory), typeof(TestFactory), new Dictionary<string, object> {{"i", 9}}, null, true);
            loader.Resolve<TestFactory>().GetInstanceNumber().Should().Be(9);

            loader.Register(typeof(ITestFactoryProxy), typeof(TestFactoryProxy), null, null, false);
            loader.Resolve<ITestFactoryProxy>().GetInstanceNumber().Should().Be(9);
            loader.Resolve<TestFactoryProxy>().GetInstanceNumber().Should().Be(9);
        }

        [Fact]
        public void ShouldVerifySameTypeResolutionNonReset()
        {
            var loader = ServiceLoader.CreateLoader();
            loader.Register(typeof(ITestFactory), typeof(TestFactory), new Dictionary<string, object> {{"i", -1}}, null, false);
            loader.Register(typeof(ITestFactoryProxy), typeof(TestFactoryProxy), null, null, false);
            loader.Resolve<ITestFactoryProxy>().GetInstanceNumber().Should().Be(loader.Resolve<TestFactoryProxy>().GetInstanceNumber() - 1);
            ;
        }
    }
}