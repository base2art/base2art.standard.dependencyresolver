﻿namespace Base2art.ComponentModel.Composition.Fixtures
{
    public class Implementation1 : IInterface1
    {
        private readonly IInterface2 interface2;

        public Implementation1(IInterface2 interface2)
        {
            this.interface2 = interface2;
        }
    }
}