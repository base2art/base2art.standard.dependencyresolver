﻿namespace Base2art.ComponentModel.Composition.Fixtures
{
    public class TestFactoryGrouper : ITestFactoryGrouper
    {
        private readonly ITestFactory factory;

        private readonly ITestFactoryProxy proxy;

        public TestFactoryGrouper(ITestFactory factory, ITestFactoryProxy proxy)
        {
            this.proxy = proxy;
            this.factory = factory;
        }
    }
}