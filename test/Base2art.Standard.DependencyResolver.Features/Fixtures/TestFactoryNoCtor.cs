﻿namespace Base2art.ComponentModel.Composition.Fixtures
{
    public class TestFactoryNoCtor : ITestFactory
    {
        private static int counter;

        private readonly int instanceNumber = -1;

        private TestFactoryNoCtor()
        {
            counter += 1;
        }

        private TestFactoryNoCtor(int i)
        {
            this.instanceNumber = i;
            counter += 1;
        }

        public int GetInstanceNumber()
        {
            return this.instanceNumber != -1 ? this.instanceNumber : counter;
        }
    }
}