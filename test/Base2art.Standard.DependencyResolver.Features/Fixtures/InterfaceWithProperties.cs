namespace Base2art.ComponentModel.Composition.Fixtures
{
    public interface InterfaceWithProperties
    {
        string[] Name { get; }
    }
}