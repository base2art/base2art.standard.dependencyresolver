﻿namespace Base2art.ComponentModel.Composition.Fixtures
{
    public class AlternateTestFactory : ITestFactory
    {
        public int GetInstanceNumber()
        {
            return 42;
        }
    }
}