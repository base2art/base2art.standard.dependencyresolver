﻿namespace Base2art.ComponentModel.Composition.Fixtures
{
    public interface ITestFactory
    {
        int GetInstanceNumber();
    }
}