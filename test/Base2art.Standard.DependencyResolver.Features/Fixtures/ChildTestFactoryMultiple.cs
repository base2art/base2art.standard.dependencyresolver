﻿namespace Base2art.ComponentModel.Composition.Fixtures
{
    public class ChildTestFactoryMultiple : IChildTestFactory
    {
        private readonly ITestFactory[] testFactories;

        public ChildTestFactoryMultiple(ITestFactory[] testFactories)
        {
            this.testFactories = testFactories;
        }

        public string GetInstanceName()
        {
            var combined = string.Empty;
            foreach (var factory in this.testFactories)
            {
                combined += factory.GetInstanceNumber().ToString();
            }

            return combined;
        }
    }
}