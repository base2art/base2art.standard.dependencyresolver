﻿namespace Base2art.ComponentModel.Composition.Fixtures
{
    public class Implementation2 : IInterface2
    {
        private readonly IInterface1 interface1;

        public Implementation2(IInterface1 interface1)
        {
            this.interface1 = interface1;
        }
    }
}