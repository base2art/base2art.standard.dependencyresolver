namespace Base2art.ComponentModel.Composition.Fixtures.SpecificExample
{
    using System.Collections.Generic;

    public class MultiConstructorClass
    {
        public MultiConstructorClass(ILogger logger)
        {
            this.Logger = logger;
        }

        public MultiConstructorClass(ILogger logger, Dictionary<string, string> parameters)
        {
            this.Logger = logger;
            this.Parameters = parameters;
        }

        public ILogger Logger { get; }

        public Dictionary<string, string> Parameters { get; } = new Dictionary<string, string>();
    }
}