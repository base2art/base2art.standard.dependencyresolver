namespace Base2art.ComponentModel.Composition.Fixtures.SpecificExample
{
    using System.Collections.Generic;

    public class SingleConstructorClass
    {
        public SingleConstructorClass(ILogger logger, int? i = null, Dictionary<string, string> parameters = null)
        {
            this.Logger = logger;
            this.Parameters = parameters ?? new Dictionary<string, string>();
        }

        public ILogger Logger { get; }

        public Dictionary<string, string> Parameters { get; }
    }
}