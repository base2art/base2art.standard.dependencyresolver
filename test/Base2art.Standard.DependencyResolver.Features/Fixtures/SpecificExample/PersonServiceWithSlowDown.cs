﻿namespace Base2art.ComponentModel.Composition.Fixtures.SpecificExample
{
    using System;

    public class PersonServiceWithSlowDown
    {
        public static int i;

        public PersonServiceWithSlowDown(IUserService userService, ICacheService cache)
        {
            i += 1;
            var start = DateTime.Now;
            while (DateTime.Now < start.AddMilliseconds(1000))
            {
//                Console.WriteLine("Waiting on " + i);
            }
        }
    }
}