﻿namespace Base2art.ComponentModel.Composition.Fixtures
{
    public interface ITestFactoryProxy
    {
        int GetInstanceNumber();
    }
}