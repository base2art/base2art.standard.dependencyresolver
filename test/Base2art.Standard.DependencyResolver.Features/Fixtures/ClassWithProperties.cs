namespace Base2art.ComponentModel.Composition.Fixtures
{
    public class ClassWithProperties : InterfaceWithProperties
    {
        public string[] Name { get; } = new string[] {"Hello"};
    }
}