﻿namespace Base2art.ComponentModel.Composition.Fixtures
{
    public interface IChildTestFactory
    {
        string GetInstanceName();
    }
}