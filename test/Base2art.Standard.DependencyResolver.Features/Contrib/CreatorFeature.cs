﻿namespace Base2art.ComponentModel.Composition.Contrib
{
    using System;
    using System.Collections.Generic;
    using Fixtures.SpecificExample;
    using FluentAssertions;
    using Xunit;

    public class CreatorFeature
    {
        private readonly IDictionary<string, object> parms = new Dictionary<string, object>();
        private readonly IDictionary<string, object> props = new Dictionary<string, object>();

        [Theory]
        [InlineData(true)]
        [InlineData(false)]
        public void ShouldCreateProperties_MissingProperties(bool nulls)
        {
            var resolver = ServiceLoader.CreateLoader();

            resolver.Bind<MissingPropVerifier<string>>()
                    .To(x => x.CreateFrom<MissingPropVerifier<string>>(!nulls ? this.parms : null, !nulls ? this.props : null));

            Action a = () => resolver.Resolve<MissingPropVerifier<string>>();
            a.Should().NotThrow();
            resolver.Resolve<MissingPropVerifier<string>>().Data.Should().BeNull();
        }

        [Theory]
        [InlineData(typeof(int?), true)]
        [InlineData(typeof(double?), true)]
        [InlineData(typeof(double), false)]
        [InlineData(typeof(string), false)]
        [InlineData(typeof(NoCtor), false)]
        [InlineData(typeof(IService1), false)]
        [InlineData(typeof(Service1), false)]
        public void Should_GetIsNullableType(Type type, bool isNullableType)
        {
            type.IsNullableValueType().Should().Be(isNullableType);
        }

        private class NoCtor
        {
            private NoCtor()
            {
            }
        }

        private class MissingFieldVerifier<T>
        {
            public MissingFieldVerifier(T data)
            {
                this.Data = data;
            }

            public T Data { get; }
        }

        public interface IMissingPropVerifier<T>
        {
            T Data { get; }
        }

        private class MissingPropVerifier<T> : IMissingPropVerifier<T>
        {
            public T Data { get; set; }
        }

        private interface IService1
        {
            Service2 Inner { get; }

            string Name { get; }
        }

        private class Service1 : IService1
        {
            public Service1(Service2 inner, string name)
            {
                this.Inner = inner;
                this.Name = name;
            }

            public Service2 Inner { get; }

            public string Name { get; }
        }

        private class Service2
        {
            public Service2(string name)
            {
                this.Name = name;
            }

            public string Name { get; }
        }

        public class NonStringable
        {
            public override string ToString()
            {
                throw new FormatException("");
            }
        }

        [Fact]
        public void ShouldCreateAnonymousObject_Instance()
        {
            var resolver = ServiceLoader.CreateLoader();

            var testObject = new {dsd = 10};
            this.props["dsd"] = "234";
            resolver.Register(testObject.GetType(), testObject.GetType(), this.props, null, false);

            dynamic obj = resolver.Resolve(testObject.GetType());
            int rez = obj.dsd;
            rez.Should().Be(234);
        }

        [Fact]
        public void ShouldCreateAnonymousObject_Singleton()
        {
            var resolver = ServiceLoader.CreateLoader();

            var testObject = new {dsd = 10};
            this.props["dsd"] = "234";
            resolver.Register(testObject.GetType(), testObject.GetType(), this.props, null, true);

            dynamic obj = resolver.Resolve(testObject.GetType());
            int rez = obj.dsd;
            rez.Should().Be(234);
        }

        [Fact]
        public void ShouldCreateObjectWithNoCtor_Fails()
        {
            var resolver = ServiceLoader.CreateLoader();

            resolver.Bind<NoCtor>().To(x => x.CreateFrom<NoCtor>(null, null));

            Action a = () => resolver.Resolve<NoCtor>();
            a.Should().Throw<MissingMethodException>();
        }

        [Fact]
        public void ShouldCreateProperties_Properties_Has()
        {
            var resolver = ServiceLoader.CreateLoader();
            this.props["Data"] = "abc";
            resolver.Bind<MissingPropVerifier<string>>()
                    .To(x => x.CreateFrom<MissingPropVerifier<string>>(this.parms, this.props));

            Action a = () => resolver.Resolve<MissingPropVerifier<string>>();
            a.Should().NotThrow();
            resolver.Resolve<MissingPropVerifier<string>>().Data.Should().Be("abc");
        }

        [Fact]
        public void ShouldCreateProperties_Properties_Has_Case()
        {
            var resolver = ServiceLoader.CreateLoader();
            this.props["Data"] = "abc";
            resolver.Bind<MissingPropVerifier<string>>()
                    .To(x => x.CreateFrom<MissingPropVerifier<string>>(this.parms, this.props));

            Action a = () => resolver.Resolve<MissingPropVerifier<string>>();
            a.Should().NotThrow();
            resolver.Resolve<MissingPropVerifier<string>>().Data.Should().Be("abc");
        }

        [Fact]
        public void ShouldNotResolveMissingField_IsNull_NonStringable()
        {
            var resolver = ServiceLoader.CreateLoader();

            this.parms["data"] = null;
            resolver.Bind<MissingFieldVerifier<NonStringable>>()
                    .To(x => x.CreateFrom<MissingFieldVerifier<NonStringable>>(this.parms, this.props));

            Action a = () => resolver.Resolve<MissingFieldVerifier<NonStringable>>();
            a.Should().NotThrow();
        }

        [Fact]
        public void ShouldNotResolveMissingField_MalFormed_Int()
        {
            var resolver = ServiceLoader.CreateLoader();

            this.parms["data"] = "234234dfsdf";
            resolver.Bind<MissingFieldVerifier<int>>()
                    .To(x => x.CreateFrom<MissingFieldVerifier<int>>(this.parms, this.props));

            Action a = () => resolver.Resolve<MissingFieldVerifier<int>>();
            var inner = a.Should().Throw<Exception>().And.InnerException;
            inner.Should().BeOfType(typeof(FormatException));
        }

        [Fact]
        public void ShouldNotResolveMissingField_MalFormed_NonStringable()
        {
            var resolver = ServiceLoader.CreateLoader();

            this.parms["data"] = "sdf";
            resolver.Bind<MissingFieldVerifier<NonStringable>>()
                    .To(x => x.CreateFrom<MissingFieldVerifier<NonStringable>>(this.parms, this.props));

            Action a = () => resolver.Resolve<MissingFieldVerifier<NonStringable>>();
            var inner = a.Should().Throw<Exception>();
        }

        [Fact]
        public void ShouldNotResolveMissingField_MalFormed_NullableInt()
        {
            var resolver = ServiceLoader.CreateLoader();

            this.parms["data"] = "234234dfsdf";
            resolver.Bind<MissingFieldVerifier<int?>>()
                    .To(x => x.CreateFrom<MissingFieldVerifier<int?>>(this.parms, this.props));

            Action a = () => resolver.Resolve<MissingFieldVerifier<int?>>();
            var inner = a.Should().Throw<Exception>().And.InnerException;
            inner.Should().BeOfType(typeof(FormatException));
        }

        [Fact]
        public void ShouldNotResolveMissingField_MalFormed_String()
        {
            var resolver = ServiceLoader.CreateLoader();

            this.parms["data"] = new NonStringable();
            resolver.Bind<MissingFieldVerifier<string>>()
                    .To(x => x.CreateFrom<MissingFieldVerifier<string>>(this.parms, this.props));

            Action a = () => resolver.Resolve<MissingFieldVerifier<string>>();
            var inner = a.Should().Throw<Exception>();
        }

        [Fact]
        public void ShouldNotResolveMissingField_Missing_Int()
        {
            var resolver = ServiceLoader.CreateLoader();

            resolver.Bind<MissingFieldVerifier<int>>()
                    .To(x => x.CreateFrom<MissingFieldVerifier<int>>(this.parms, this.props));

            Action a = () => resolver.Resolve<MissingFieldVerifier<int>>();
            a.Should().Throw<BindingException>();
        }

        [Fact]
        public void ShouldNotResolveMissingField_Missing_NonStringable()
        {
            var resolver = ServiceLoader.CreateLoader();

            resolver.Bind<MissingFieldVerifier<NonStringable>>()
                    .To(x => x.CreateFrom<MissingFieldVerifier<NonStringable>>(this.parms, this.props));

            Action a = () => resolver.Resolve<MissingFieldVerifier<NonStringable>>();
            a.Should().Throw<KeyNotFoundException>();
        }

        [Fact]
        public void ShouldNotResolveMissingField_Missing_NullableInt()
        {
            var resolver = ServiceLoader.CreateLoader();

            resolver.Bind<MissingFieldVerifier<int?>>()
                    .To(x => x.CreateFrom<MissingFieldVerifier<int?>>(this.parms, this.props));

            Action a = () => resolver.Resolve<MissingFieldVerifier<int?>>();
            a.Should().Throw<BindingException>();
        }

        [Fact]
        public void ShouldNotResolveMissingField_Missing_String()
        {
            var resolver = ServiceLoader.CreateLoader();

            resolver.Bind<MissingFieldVerifier<string>>()
                    .To(x => x.CreateFrom<MissingFieldVerifier<string>>(this.parms, this.props));

            Action a = () => resolver.Resolve<MissingFieldVerifier<string>>();
            a.Should().Throw<BindingException>();
        }

        [Fact]
        public void ShouldNotResolveMissingField_Success_Int()
        {
            var resolver = ServiceLoader.CreateLoader();

            this.parms["data"] = "234234";
            resolver.Bind<MissingFieldVerifier<int>>()
                    .To(x => x.CreateFrom<MissingFieldVerifier<int>>(this.parms, this.props));

            Func<MissingFieldVerifier<int>> a = () => resolver.Resolve<MissingFieldVerifier<int>>();
            a().Data.Should().Be(234234);
        }

        [Fact]
        public void ShouldNotResolveMissingField_Success_NonStringable()
        {
            var resolver = ServiceLoader.CreateLoader();

            this.parms["data"] = new NonStringable();
            resolver.Bind<MissingFieldVerifier<NonStringable>>()
                    .To(x => x.CreateFrom<MissingFieldVerifier<NonStringable>>(this.parms, this.props));

            Func<MissingFieldVerifier<NonStringable>> a = () => resolver.Resolve<MissingFieldVerifier<NonStringable>>();
            a().Data.Should().Be(this.parms["data"]);
        }

        [Fact]
        public void ShouldNotResolveMissingField_Success_NullableInt()
        {
            var resolver = ServiceLoader.CreateLoader();

            this.parms["data"] = "234234";
            resolver.Bind<MissingFieldVerifier<int?>>()
                    .To(x => x.CreateFrom<MissingFieldVerifier<int?>>(this.parms, this.props));

            Func<MissingFieldVerifier<int?>> a = () => resolver.Resolve<MissingFieldVerifier<int?>>();
            a().Data.Should().Be(234234);
        }

        [Fact]
        public void ShouldNotResolveMissingField_Success_String()
        {
            var resolver = ServiceLoader.CreateLoader();

            this.parms["data"] = "234234";
            resolver.Bind<MissingFieldVerifier<string>>()
                    .To(x => x.CreateFrom<MissingFieldVerifier<string>>(this.parms, this.props));

            Func<MissingFieldVerifier<string>> a = () => resolver.Resolve<MissingFieldVerifier<string>>();
            a().Data.Should().Be("234234");
        }

//        public CreatorFeature()
//        {
//            parms.Clear();
//            props.Clear();
//        }

        [Fact]
        public void ShouldResolve()
        {
            var resolver = ServiceLoader.CreateLoader();

            this.parms["name"] = "MyName";
            var parms1 = this.parms.Copy();
            resolver.Bind<Service1>()
                    .To(x => x.CreateFrom<Service1>(parms1, this.props.Copy()));

            this.parms.Clear();
            this.parms["name"] = "OtherName";
            resolver.Bind<Service2>()
                    .To(x => x.CreateFrom<Service2>(this.parms, this.props.Copy()));

            var s = (Service1) resolver.Resolve(typeof(Service1));
            s.Inner.Name.Should().Be("OtherName");
            s.Name.Should().Be("MyName");
            resolver.Resolve(typeof(Service1));
        }

        [Fact]
        public void ShouldResolveInterface()
        {
            var resolver = ServiceLoader.CreateLoader();

            this.parms["name"] = "MyName";
            var parms1 = this.parms.Copy();
            resolver.Bind<IService1>()
                    .To(x => x.CreateFrom<Service1>(parms1, this.props.Copy()));

            this.parms.Clear();
            this.parms["name"] = "OtherName";
            resolver.Bind<Service2>()
                    .To(x => x.CreateFrom<Service2>(this.parms, this.props.Copy()));

            var s = (IService1) resolver.Resolve(typeof(IService1));
            s.Inner.Name.Should().Be("OtherName");
            s.Name.Should().Be("MyName");
            resolver.Resolve(typeof(IService1));
        }

        [Fact]
        public void ShouldResolveWithInstance_WithInterfaces()
        {
            var resolver = ServiceLoader.CreateLoader();

            resolver.RegisterInstance(new MissingPropVerifier<string> {Data = "asd"}, true);

            Func<MissingPropVerifier<string>> a = () => resolver.Resolve<MissingPropVerifier<string>>();
            a().Data.Should().Be("asd");

            Func<IMissingPropVerifier<string>> b = () => resolver.Resolve<IMissingPropVerifier<string>>();
            b().Data.Should().Be("asd");
        }

        [Fact]
        public void ShouldResolveWithInstance_WithNoInterfaces()
        {
            var resolver = ServiceLoader.CreateLoader();

            resolver.RegisterInstance(new MissingPropVerifier<string> {Data = "asd"}, false);

            Func<MissingPropVerifier<string>> a = () => resolver.Resolve<MissingPropVerifier<string>>();
            a().Data.Should().Be("asd");

            Func<IMissingPropVerifier<string>> b = () => resolver.Resolve<IMissingPropVerifier<string>>(true);
            b().Should().BeNull();
        }

        [Theory]
        [InlineData(false, false)]
        [InlineData(true, false)]
        [InlineData(false, true)]
        [InlineData(true, true)]
        public void ShouldResolveDifferentConstructors(bool registerDictionary, bool registerExtra)
        {
            var resolver = ServiceLoader.CreateLoader();

            resolver.Bind<ILogger>().ToInstanceCreator<Logger>();

            var inputParameters = new Dictionary<string, object>();

            if (registerDictionary)
            {
                inputParameters.Add("parameters", new Dictionary<string, string> {{"abc", "123"}});
            }

            if (registerExtra)
            {
                inputParameters.Add("parameters_extra", new Dictionary<string, string> {{"abc", "123"}});
            }

            var instance = resolver.CreateFrom<MultiConstructorClass>(inputParameters,
                                                                      new Dictionary<string, object>());

            instance.Parameters.Count.Should().Be(registerDictionary ? 1 : 0);
            if (registerDictionary)
            {
                instance.Parameters["abc"].Should().Be("123");
            }

            instance.Logger.Should().NotBeNull();
        }

        [Theory]
        [InlineData("i", 1)]
        [InlineData("parameters", null)]
        public void ShouldResolveConstructorMissingDictionary(string name, object value)
        {
            var resolver = ServiceLoader.CreateLoader();

            resolver.Bind<ILogger>().ToInstanceCreator<Logger>();

            var inputParameters = new Dictionary<string, object>();
            inputParameters.Add(name, value);

            var instance = resolver.CreateFrom<SingleConstructorClass>(inputParameters,
                                                                      new Dictionary<string, object>());

            instance.Parameters.Count.Should().Be(0);

            instance.Logger.Should().NotBeNull();
        }
    }
}