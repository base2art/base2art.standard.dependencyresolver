﻿namespace Base2art.ComponentModel.Composition.Contrib
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Fixtures.SpecificExample;
    using FluentAssertions;
    using Xunit;

    public class InterfaceBasedCreatorFeature
    {
        private readonly Dictionary<string, object> parms = new Dictionary<string, object>();

        private readonly Dictionary<string, object> props = new Dictionary<string, object>();

        //        [Fact]
        //        public void ShouldResolveInterface1()
        //        {
        //            var resolver = ServiceLoader.CreateLoader();
        //
        //            resolver.Bind<IService1>()
        //                .To(x => x.CreateFrom<IService1>(new Dictionary<string, object> { {
        //                                                         "name",
        //                                                         "MyName"
        //                                                     }
        //                                                 }));
        //
        //            resolver.Bind<Service2>()
        //                .To(x => x.CreateFrom<Service2>(new Dictionary<string, object> { {
        //                                                        "name",
        //                                                        "OtherName"
        //                                                    }
        //                                                }));
        //
        //            IService1 s = (IService1)resolver.Resolve(typeof(IService1));
        //            Assert.Equal(s.Inner.Name, "OtherName");
        //        }
        //
        //        [Fact]
        //        public void ShouldResolveInterface2()
        //        {
        //            var resolver = ServiceLoader.CreateLoader();
        //
        //            resolver.Bind<IService1>()
        //                .To(x => x.CreateFrom<IService1>(new Dictionary<string, object> { {
        //                                                         "name",
        //                                                         "MyName"
        //                                                     }
        //                                                 }));
        //
        //            resolver.Bind<Service2>()
        //                .To(x => x.CreateFrom<Service2>(new Dictionary<string, object> { {
        //                                                        "name",
        //                                                        "OtherName"
        //                                                    }
        //                                                }));
        //            try
        //            {
        //                IService1 s = (IService1)resolver.Resolve(typeof(IService1));
        //                Assert.Equal(s.Inner.Name, "OtherName");
        //            }
        //            catch (Exception)
        //            {
        //            }
        //
        //            IService1 s1 = (IService1)resolver.Resolve(typeof(IService1));
        //            Assert.Equal(s1.Inner.Name, "OtherName");
        //        }
        private interface IService1
        {
            IService2 Inner { get; }

            string Name { get; }
        }

        private interface IService2
        {
            string Name { get; }
        }

        private class Service1 : IService1
        {
            public Service1(IService2 inner, string name)
            {
                this.Inner = inner;
                this.Name = name;
            }

            public IService2 Inner { get; }

            public string Name { get; }
        }

        private class ServiceTakesArray1 // : IService1
        {
            public ServiceTakesArray1(IService2[] inner, string name)
            {
                this.Inner = inner;
                this.Name = name;
            }

            public IService2[] Inner { get; }

            public string Name { get; }
        }

        private class ServiceTakesArray2 // : IService1
        {
            private readonly Service2[] inner;

            public ServiceTakesArray2(Service2[] inner, string name)
            {
                this.inner = inner;
                this.Name = name;
            }

            public IService2[] Inner => this.inner;

            public string Name { get; }
        }

        private class Service2 : IService2
        {
            public Service2(string name)
            {
                this.Name = name;
            }

            public string Name { get; }
        }

        [Fact]
        public async void AsyncTry()
        {
            if (Environment.ProcessorCount <= 1)
            {
                Assert.False(true, "This test requires 2 cpus.");
            }

            var resolver = ServiceLoader.CreateLoader();

            resolver.Register(typeof(ILogger), typeof(Logger), this.parms, this.props, false);
            resolver.Register(typeof(IUserService), typeof(UserService), this.parms, this.props, false);
            resolver.Register(typeof(ICacheService), typeof(CacheService), this.parms, this.props, false);
            resolver.Register(typeof(PersonServiceWithSlowDown), typeof(PersonServiceWithSlowDown), this.parms, this.props, false);

            var call = new Func<object>(() => resolver.Resolve(typeof(PersonServiceWithSlowDown)));

            var task1 = Task.Run(call);
            var task2 = Task.Run(call);
            await task2;
            await task1;
        }

        [Fact]
        public void ShouldRegisterAndResolve()
        {
            var resolver = ServiceLoader.CreateLoader();

            this.parms["name"] = "MyName";
            var parms1 = this.parms.Copy();
            this.parms.Clear();
            this.parms["name"] = "OtherName";

            resolver.Register(typeof(IService1), typeof(Service1), parms1, this.props, false);
            resolver.Register(typeof(IService2), typeof(Service2), this.parms, this.props, false);

            var s = (IService1) resolver.Resolve(typeof(IService1));
            s.Inner.Name.Should().Be("OtherName");
            s.Name.Should().Be("MyName");
            s = (IService1) resolver.Resolve(typeof(IService1));
            s.Inner.Name.Should().Be("OtherName");
            s.Name.Should().Be("MyName");

            var s1 = (IService2) resolver.Resolve(typeof(IService2));
            s1.Name.Should().Be("OtherName");
        }

        [Fact]
        public void ShouldRegisterAndResolve_SpecificExample()
        {
            var resolver = ServiceLoader.CreateLoader();

            resolver.Register(typeof(ILogger), typeof(Logger), this.parms, this.props, false);
            resolver.Register(typeof(IUserService), typeof(UserService), this.parms, this.props, false);
            resolver.Register(typeof(ICacheService), typeof(CacheService), this.parms, this.props, false);
            resolver.Register(typeof(PersonService), typeof(PersonService), this.parms, this.props, false);

            new Action(() => resolver.Resolve(typeof(PersonService)))(); //.ShouldNotThrow<CircularDependencyException>();
            new Action(() => resolver.Resolve(typeof(PersonService)))(); //.ShouldNotThrow<CircularDependencyException>();

            //            Creator.CreateFromInternal(
            //                typeof(PersonService),
            //                resolver,
            //                (x) => false,
            //                (x) => null);
        }

        [Fact]
        public void ShouldRegisterAndResolve_SpecificExample_1()
        {
            var resolver = ServiceLoader.CreateLoader();

            resolver.Register(typeof(ILogger), typeof(Logger), this.parms, this.props, false);
            resolver.Register(typeof(IUserService), typeof(UserService), this.parms, this.props, false);
            resolver.Register(typeof(ICacheService), typeof(CacheService), this.parms, this.props, false);
            //            resolver.Register(typeof(PersonService), typeof(PersonService), parms, props, false);

            //            new Action(() => resolver.Resolve(typeof(PersonService)))();//.ShouldNotThrow<CircularDependencyException>();
            //            new Action(() => resolver.Resolve(typeof(PersonService)))();//.ShouldNotThrow<CircularDependencyException>();
            resolver.Bind<PersonService>()
                    .To(x => x.CreateFrom<PersonService>(new Dictionary<string, object>(), new Dictionary<string, object>()));
            //            DynamicRegistrar.CreateFrom(
            //                var p = Creator.CreateFromInternal(
            //                    typeof(PersonService),
            //                    resolver,
            //                    (x) => false,
            //                    (x) => null);
            resolver.Resolve<PersonService>().Should().NotBeNull();
        }

        [Fact]
        public void ShouldRegisterAndResolveArray()
        {
            var resolver = ServiceLoader.CreateLoader();

            this.parms["name"] = "MyName";
            var parms1 = this.parms.Copy();
            this.parms.Clear();
            this.parms["name"] = "OtherName 1";
            var parms2 = this.parms.Copy();
            this.parms.Clear();
            this.parms["name"] = "OtherName 2";
            var parms3 = this.parms.Copy();

            resolver.Register(typeof(ServiceTakesArray1), typeof(ServiceTakesArray1), parms1, this.props, false);
            resolver.Register(typeof(IService2), typeof(Service2), parms2, this.props, false);
            resolver.Register(typeof(IService2), typeof(Service2), parms3, this.props, false);

            var s = resolver.Resolve<ServiceTakesArray1>();
            s.Inner.Should().HaveCount(2);
            s.Inner[0].Name.Should().Be("OtherName 1");
            s.Inner[1].Name.Should().Be("OtherName 2");
            s.Name.Should().Be("MyName");

            s = resolver.Resolve<ServiceTakesArray1>();
            s.Inner[0].Name.Should().Be("OtherName 1");
            s.Inner[1].Name.Should().Be("OtherName 2");
            s.Name.Should().Be("MyName");
        }

        [Fact]
        public void ShouldRegisterAndResolveArrayConcrete()
        {
            var resolver = ServiceLoader.CreateLoader();

            this.parms["name"] = "MyName";
            var parms1 = this.parms.Copy();
            this.parms.Clear();
            this.parms["name"] = "OtherName 1";
            var parms2 = this.parms.Copy();
            this.parms.Clear();
            this.parms["name"] = "OtherName 2";
            var parms3 = this.parms.Copy();

            resolver.Register(typeof(ServiceTakesArray2), typeof(ServiceTakesArray2), parms1, this.props, false);
            resolver.Register(typeof(Service2), typeof(Service2), parms2, this.props, false);
            resolver.Register(typeof(Service2), typeof(Service2), parms3, this.props, false);

            var s = resolver.Resolve<ServiceTakesArray2>();
            s.Inner.Should().HaveCount(2);
            s.Inner[0].Name.Should().Be("OtherName 1");
            s.Inner[1].Name.Should().Be("OtherName 2");
            s.Name.Should().Be("MyName");

            s = resolver.Resolve<ServiceTakesArray2>();
            s.Inner[0].Name.Should().Be("OtherName 1");
            s.Inner[1].Name.Should().Be("OtherName 2");
            s.Name.Should().Be("MyName");
        }

//        public InterfaceBasedCreatorFeature()
//        {
//            props.Clear();
//        }

        [Fact]
        public void ShouldResolve()
        {
            var resolver = ServiceLoader.CreateLoader();
            this.parms["name"] = "MyName";
            var parms1 = this.parms.Copy();
            resolver.Bind<IService1>().To(x => x.CreateFrom<Service1>(parms1, this.props.Copy()));
            this.parms.Clear();
            this.parms["name"] = "OtherName";
            resolver.Bind<IService2>().To(x => x.CreateFrom<Service2>(this.parms, this.props.Copy()));
            var s = (IService1) resolver.Resolve(typeof(IService1));
            s.Inner.Name.Should().Be("OtherName");
            s.Name.Should().Be("MyName");
        }

        [Fact]
        public void ShouldResolveInterface()
        {
            var resolver = ServiceLoader.CreateLoader();
            this.parms["name"] = "MyName";
            var parms1 = this.parms.Copy();
            resolver.Bind<IService1>().To(x => x.CreateFrom<Service1>(parms1, this.props.Copy()));
            this.parms.Clear();
            this.parms["name"] = "OtherName";
            resolver.Bind<IService2>().To(x => x.CreateFrom<Service2>(this.parms, this.props.Copy()));
            var s = (IService1) resolver.Resolve(typeof(IService1));
            s.Inner.Name.Should().Be("OtherName");
            s.Name.Should().Be("MyName");
        }
    }
}